package com.demo.zyy.pattern.strategy.definition;

/**
 * 策略抽象（算法抽象）
 */
public interface Strategy {
    void calculate();
}
