package com.demo.zyy.pattern.strategy.duck.v3;

/**
 * 鸭子抽象类
 *  所有的鸭子都应该继承至该类
 */
public abstract class DuckV3 {

    //飞行行为
    FlyBehavior flyBehavior;

    //叫的行为
    QuackBehavior quackBehavior;

    /**
     * 抽象的游泳方法
     *  所有的鸭子都是在水里面游泳的。
     */
    public void swim(){
        System.out.println("在水里游泳");
    }

    public void fly(){
        flyBehavior.fly();
    }

    //鸭子的叫委托给行为抽象了。
    public void quack(){
        quackBehavior.quack();
    }

    /**
     * 鸭子外观
     *  不同的鸭子有不同的外观
     */
    public abstract void display();
}
