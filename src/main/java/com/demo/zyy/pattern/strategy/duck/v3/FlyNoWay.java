package com.demo.zyy.pattern.strategy.duck.v3;

/**
 * 不能飞
 */
public class FlyNoWay implements FlyBehavior {
    @Override
    public void fly() {
        System.out.println("飞不动...");
    }
}
