package com.demo.zyy.pattern.strategy.duck.v2;

/**
 * 橡皮鸭
 */
public class RubberDuckV2 extends DuckV2 implements Quackable {
    @Override
    public void display() {
        System.out.println("橡皮鸭好像都是黄色的");
    }

    @Override
    public void quack() {
        System.out.println("橡皮鸭是吱吱吱的叫");
    }
}
