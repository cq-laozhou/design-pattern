package com.demo.zyy.pattern.strategy.sort.v1;

import java.util.List;

/**
 * 排序管理器
 */
public class SortingManagerV1 {
    List list;

    public SortingManagerV1(List list) {
        this.list = list;
    }

    public void sortListBaseOnType(SortingTypeV1 type){
        System.out.println("===================================");
        System.out.println("Sorting List based on Type");
        System.out.println("===================================");

        if(type == SortingTypeV1.MERGE_SORT){
            sortListUsingMergeSort();
        }else if(type == SortingTypeV1.QUICK_SORT){
            sortListUsingQuickSort();
        }else {
            throw new RuntimeException("not support type");
        }
    }

    private void sortListUsingQuickSort() {
        System.out.println("Sorting List using quick sort");
    }

    private void sortListUsingMergeSort() {
        System.out.println("Sorting List using merge sort");
    }


}
