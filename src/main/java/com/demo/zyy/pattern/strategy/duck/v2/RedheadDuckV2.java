package com.demo.zyy.pattern.strategy.duck.v2;

/**
 * 红头鸭
 */
public class RedheadDuckV2 extends DuckV2 implements Flyable,Quackable {
    @Override
    public void display() {
        System.out.println("红头鸭的头是红色的");
    }

    @Override
    public void fly() {
        System.out.println("野鸭用翅膀在天上飞");
    }

    @Override
    public void quack() {
        System.out.println("呱呱呱");
    }
}
