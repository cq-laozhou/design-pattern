package com.demo.zyy.pattern.strategy.duck.v1;

/**
 * 红头鸭
 */
public class RedheadDuckV1 extends DuckV1{
    @Override
    public void display() {
        System.out.println("红头鸭的头是红色的");
    }


}
