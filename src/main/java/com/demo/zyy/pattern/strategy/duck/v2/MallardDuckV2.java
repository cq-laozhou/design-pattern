package com.demo.zyy.pattern.strategy.duck.v2;


/**
 * 野鸭
 */
public class MallardDuckV2 extends DuckV2 implements Flyable,Quackable {
    @Override
    public void display() {
        System.out.println("野鸭的外观是绿头");
    }

    @Override
    public void fly() {
        System.out.println("野鸭用翅膀在天上飞");
    }

    @Override
    public void quack() {
        System.out.println("呱呱呱");
    }
}
