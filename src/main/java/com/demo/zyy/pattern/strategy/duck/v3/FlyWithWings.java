package com.demo.zyy.pattern.strategy.duck.v3;

/**
 * 使用翅膀飞
 */
public class FlyWithWings implements FlyBehavior {
    @Override
    public void fly() {
        System.out.println("煽动翅膀，飞向天空...");
    }
}
