package com.demo.zyy.pattern.strategy.duck.v3;

/**
 * 火箭助力
 */
public class FlyRocketPower implements FlyBehavior {
    @Override
    public void fly() {
        System.out.println("火箭助力飞行...");
    }
}
