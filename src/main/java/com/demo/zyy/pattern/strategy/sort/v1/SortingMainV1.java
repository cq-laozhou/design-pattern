package com.demo.zyy.pattern.strategy.sort.v1;

import java.util.Arrays;
import java.util.List;

/**
 * 客户
 */
public class SortingMainV1 {
    public static void main(String[] args) {
        List list = Arrays.asList(new Integer[]{44,5,3,5,5,64,3});

        SortingManagerV1 sm = new SortingManagerV1(list);
        // Sorting using merge sort
        sm.sortListBaseOnType(SortingTypeV1.MERGE_SORT);

        System.out.println();
        // Sorting using quick sort
        sm.sortListBaseOnType(SortingTypeV1.QUICK_SORT);
    }
}
