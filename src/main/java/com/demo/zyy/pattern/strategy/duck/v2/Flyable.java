package com.demo.zyy.pattern.strategy.duck.v2;

/**
 * 飞行接口
 */
public interface Flyable {
    void fly();
}
