package com.demo.zyy.pattern.strategy.sort.v2;

import java.util.List;

/**
 * 归并排序算法
 */
public class MergeSortStrategy implements SortingStrategy{
    @Override
    public void sortList(List list) {
        System.out.println("Sorting List using merge sort");
    }
}
