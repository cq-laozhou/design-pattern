package com.demo.zyy.pattern.strategy.duck.v1;

/**
 * 野鸭
 */
public class MallardDuckV1 extends DuckV1 {
    @Override
    public void display() {
        System.out.println("野鸭的外观是绿头");
    }
}
