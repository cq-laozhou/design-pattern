package com.demo.zyy.pattern.strategy.duck.v1;

/**
 * 鸭子抽象类
 *  所有的鸭子都应该继承至该类
 */
public abstract class DuckV1 {

    /**
     * 抽象的游泳方法
     *  所有的鸭子都是在水里面游泳的。
     */
    public void swim(){
        System.out.println("在水里游泳");
    }

    /**
     * 鸭子叫
     *  所有的鸭子都是"呱呱呱"的叫
     */
    public void quack(){
        System.out.println("呱呱呱");
    }

    /**
     * 新添加的飞的方法
     */
    public void fly(){
        System.out.println("鸭子飞吧...");
    }

    /**
     * 鸭子外观
     *  不同的鸭子有不同的外观
     */
    public abstract void display();
}
