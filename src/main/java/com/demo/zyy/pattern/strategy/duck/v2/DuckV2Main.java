package com.demo.zyy.pattern.strategy.duck.v2;

/**
 * V2版鸭子游戏的测试
 */
public class DuckV2Main {

    public static void main(String[] args) {
        //一只野鸭在玩耍
        DuckV2 duck1 = new MallardDuckV2();
        duck1.display();
        duck1.swim();
        ((Flyable)duck1).fly();
        ((Quackable)duck1).quack();
        System.out.println();

        //一只红头鸭在玩耍
        DuckV2 duck2 = new RedheadDuckV2();
        duck2.display();
        duck2.swim();
        ((Flyable)duck2).fly();
        ((Quackable)duck2).quack();
        System.out.println();

        //一只橡皮鸭
        DuckV2 duck3 = new RubberDuckV2();
        duck3.display();
        duck3.swim();
        ((Quackable)duck3).quack();

    }
}
