package com.demo.zyy.pattern.strategy.duck.v3;

public class Quack implements QuackBehavior {
    @Override
    public void quack() {
        System.out.println("呱呱呱的叫");
    }
}
