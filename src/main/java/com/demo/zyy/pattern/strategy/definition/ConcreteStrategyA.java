package com.demo.zyy.pattern.strategy.definition;

/**
 * 具体策略A
 */
public class ConcreteStrategyA implements Strategy {
    @Override
    public void calculate() {
        System.out.println("策略A的算法");
    }
}
