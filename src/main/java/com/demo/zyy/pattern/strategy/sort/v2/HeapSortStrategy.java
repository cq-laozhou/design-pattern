package com.demo.zyy.pattern.strategy.sort.v2;

import java.util.List;

/**
 * 堆排序算法
 */
public class HeapSortStrategy implements SortingStrategy{
    @Override
    public void sortList(List list) {
        System.out.println("Sorting List using heap sort");
    }
}
