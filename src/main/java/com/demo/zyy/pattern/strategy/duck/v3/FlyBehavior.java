package com.demo.zyy.pattern.strategy.duck.v3;

/**
 * 飞行行为
 */
public interface FlyBehavior {
    void fly();
}
