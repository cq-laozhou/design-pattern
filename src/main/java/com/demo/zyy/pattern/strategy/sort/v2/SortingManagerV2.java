package com.demo.zyy.pattern.strategy.sort.v2;

import java.util.List;

/**
 * 使用策略模式解决
 */
public class SortingManagerV2 {

    SortingStrategy sortingStrategy;
    List<Integer> list;

    public SortingManagerV2(SortingStrategy sortingStrategy, List<Integer> list) {
        this.sortingStrategy = sortingStrategy;
        this.list = list;
    }

    public void sortingList(){
        System.out.println("===================================");
        System.out.println("Sorting List based on Strategy");
        System.out.println("===================================");

        sortingStrategy.sortList(list);
    }

    public SortingStrategy getSortingStrategy() {
        return sortingStrategy;
    }

    public void setSortingStrategy(SortingStrategy sortingStrategy) {
        this.sortingStrategy = sortingStrategy;
    }
}
