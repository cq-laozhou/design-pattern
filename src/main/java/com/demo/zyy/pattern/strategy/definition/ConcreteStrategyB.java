package com.demo.zyy.pattern.strategy.definition;

/**
 * 具体策略B
 */
public class ConcreteStrategyB implements Strategy {
    @Override
    public void calculate() {
        System.out.println("策略B的算法");
    }
}
