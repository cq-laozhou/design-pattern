package com.demo.zyy.pattern.strategy.sort.v2;

import java.util.List;

/**
 * 快速排序算法
 */
public class QuickSortStrategy implements SortingStrategy{
    @Override
    public void sortList(List list) {
        System.out.println("Sorting List using quick sort");
    }
}
