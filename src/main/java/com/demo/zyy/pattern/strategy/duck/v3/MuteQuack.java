package com.demo.zyy.pattern.strategy.duck.v3;

public class MuteQuack implements QuackBehavior {

    @Override
    public void quack() {
        //不会叫
    }
}
