package com.demo.zyy.pattern.strategy.duck.v3;

/**
 * 橡皮鸭
 */
public class RubberDuckV3 extends DuckV3{

    public RubberDuckV3() {
        flyBehavior = new FlyNoWay();
        quackBehavior = new Squeak();
    }

    @Override
    public void display() {
        System.out.println("橡皮鸭好像都是黄色的");
    }
}
