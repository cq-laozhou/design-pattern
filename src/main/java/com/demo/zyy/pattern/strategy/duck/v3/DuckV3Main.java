package com.demo.zyy.pattern.strategy.duck.v3;

/**
 * V3版鸭子游戏的测试
 */
public class DuckV3Main {

    public static void main(String[] args) {
        //一只野鸭在玩耍
        DuckV3 duck1 = new MallardDuckV3();
        duck1.display();
        duck1.fly();
        duck1.quack();
        System.out.println();

        //一只红头鸭在玩耍
        DuckV3 duck2 = new RedheadDuckV3();
        duck2.display();
        duck2.swim();
        duck2.quack();
        duck2.fly();
        System.out.println();

        //一只橡皮鸭
        DuckV3 duck3 = new RubberDuckV3();
        duck3.display();
        duck3.fly();
        duck3.quack();
        System.out.println();

        //一只模型鸭
        DuckV3 duck4 = new ModelDuckV3();
        duck4.display();
        duck4.fly();
        duck4.quack();
    }
}
