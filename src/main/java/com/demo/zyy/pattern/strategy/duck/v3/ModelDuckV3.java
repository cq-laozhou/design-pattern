package com.demo.zyy.pattern.strategy.duck.v3;

/**
 * 新增一个模型鸭
 */
public class ModelDuckV3 extends DuckV3{

    public ModelDuckV3() {
        //模型鸭不会飞，但装了火箭推进器
        flyBehavior = new FlyRocketPower();
        quackBehavior = new MuteQuack();
    }

    @Override
    public void display() {
        System.out.println("我是一只模型鸭");
    }
}
