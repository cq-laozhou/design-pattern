package com.demo.zyy.pattern.strategy.duck.v3;

/**
 * 红头鸭
 */
public class RedheadDuckV3 extends DuckV3{
    public RedheadDuckV3() {
        flyBehavior = new FlyWithWings();
        quackBehavior = new Quack();
    }

    @Override
    public void display() {
        System.out.println("红头鸭的头是红色的");
    }
}
