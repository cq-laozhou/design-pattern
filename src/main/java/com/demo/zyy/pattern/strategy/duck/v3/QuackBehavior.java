package com.demo.zyy.pattern.strategy.duck.v3;

/**
 * 叫行为
 */
public interface QuackBehavior {
    void quack();
}
