package com.demo.zyy.pattern.strategy.sort.v2;

import java.util.Arrays;
import java.util.List;

/**
 * 客户
 */
public class SortingMainV2 {
    public static void main(String[] args) {
        List list = Arrays.asList(new Integer[]{44,5,3,5,5,64,3});

        SortingManagerV2 sm = new SortingManagerV2(new MergeSortStrategy(), list);
        sm.sortingList();

        System.out.println();

        sm.setSortingStrategy(new QuickSortStrategy());
        sm.sortingList();

        System.out.println();

        sm.setSortingStrategy(new HeapSortStrategy());
        sm.sortingList();
    }
}
