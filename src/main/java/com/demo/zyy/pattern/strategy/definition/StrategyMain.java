package com.demo.zyy.pattern.strategy.definition;

/**
 * 客户
 */
public class StrategyMain {
    public static void main(String[] args) {
        Context context = new Context(new ConcreteStrategyA());
        context.calculate();

        context.setStrategy(new ConcreteStrategyB());
        context.calculate();
    }
}
