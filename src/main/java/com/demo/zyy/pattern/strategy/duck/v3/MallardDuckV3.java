package com.demo.zyy.pattern.strategy.duck.v3;


/**
 * 野鸭
 */
public class MallardDuckV3 extends DuckV3 {

    public MallardDuckV3() {
        //定义野鸭的行为
        flyBehavior = new FlyWithWings();
        quackBehavior = new Quack();
    }

    @Override
    public void display() {
        System.out.println("野鸭的外观是绿头");
    }
}
