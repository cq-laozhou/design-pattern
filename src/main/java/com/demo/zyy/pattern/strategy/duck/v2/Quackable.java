package com.demo.zyy.pattern.strategy.duck.v2;

/**
 * 叫接口
 */
public interface Quackable {
    void quack();
}
