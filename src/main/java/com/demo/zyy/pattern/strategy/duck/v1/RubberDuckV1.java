package com.demo.zyy.pattern.strategy.duck.v1;

/**
 * 橡皮鸭
 */
public class RubberDuckV1 extends DuckV1 {
    @Override
    public void display() {
        System.out.println("橡皮鸭好像都是黄色的");
    }

    @Override
    public void quack() {
        System.out.println("吱吱吱");
    }

    @Override
    public void fly() {

    }
}
