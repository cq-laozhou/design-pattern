package com.demo.zyy.pattern.strategy.duck.v3;

public class Squeak implements QuackBehavior {
    @Override
    public void quack() {
        System.out.println("吱吱吱的叫");
    }
}
