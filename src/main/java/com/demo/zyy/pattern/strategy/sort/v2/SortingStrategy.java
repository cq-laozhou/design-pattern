package com.demo.zyy.pattern.strategy.sort.v2;

import java.util.List;

/**
 * 抽象排序策略
 */
public interface SortingStrategy {

    void sortList(List list);
}
