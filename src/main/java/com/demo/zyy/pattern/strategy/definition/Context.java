package com.demo.zyy.pattern.strategy.definition;

/**
 * 策略上下文，用来组合抽象策略
 */
public class Context {

    private Strategy strategy;

    public Context(Strategy strategy) {
        this.strategy = strategy;
    }

    public void calculate(){
        strategy.calculate();
    }

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }
}
