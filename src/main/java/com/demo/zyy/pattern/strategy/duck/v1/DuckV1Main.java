package com.demo.zyy.pattern.strategy.duck.v1;

/**
 * V1版鸭子游戏的测试
 */
public class DuckV1Main {

    public static void main(String[] args) {
        //一只野鸭在玩耍
        DuckV1 duck1 = new MallardDuckV1();
        duck1.display();
        duck1.fly();
        duck1.swim();
        duck1.quack();
        System.out.println();

        //一只红头鸭在玩耍
        DuckV1 duck2 = new RedheadDuckV1();
        duck2.display();
        duck2.fly();
        duck2.swim();
        duck2.quack();
        System.out.println();

        //一只橡皮鸭
        DuckV1 duck3 = new RubberDuckV1();
        duck3.display();
        duck3.fly();
        duck3.swim();
        duck3.quack();

    }
}
