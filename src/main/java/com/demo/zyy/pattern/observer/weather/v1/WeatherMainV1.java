package com.demo.zyy.pattern.observer.weather.v1;

/**
 * V1版测试
 */
public class WeatherMainV1 {
    public static void main(String[] args) {
        WeatherDataV1 weatherData = new WeatherDataV1();
        //气象数据发生变更
        weatherData.setTemperature(34.5f);
        weatherData.setHumidity(125.7f);
        weatherData.setPressure(32.4f);
        //调用我们实现的方法。
        weatherData.measurementsChanged();
    }
}
