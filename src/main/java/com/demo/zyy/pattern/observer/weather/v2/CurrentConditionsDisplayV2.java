package com.demo.zyy.pattern.observer.weather.v2;

import com.demo.zyy.pattern.observer.weather.v1.WeatherDTOV1;

/**
 * 目前状况布告板，具体观察者实现
 */
public class CurrentConditionsDisplayV2 implements DisplayV2 {
    @Override
    public void update(WeatherDTOV2 data) {
        System.out.println("目标状况布告板更新，新数据:"+data.toString());
    }
}
