package com.demo.zyy.pattern.observer.weather.v1;

/**
 * 天气预报布告板
 */
public class ForecastDisplayV1 implements DisplayV1 {
    @Override
    public void update(WeatherDTOV1 data) {
        System.out.println("天气预报布告板更新，新数据:"+data.toString());
    }
}
