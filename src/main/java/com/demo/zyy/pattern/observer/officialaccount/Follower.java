package com.demo.zyy.pattern.observer.officialaccount;

/**
 * 粉丝
 */
public class Follower implements Observer {

    private String name;

    public Follower(String name) {
        this.name = name;
    }

    @Override
    public void update(Event event) {
        System.out.println(getName() + ",接受到公众号"+ event.getOfficialAccountName() +"推送的新文章:" + event.getArticleName());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
