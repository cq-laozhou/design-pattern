package com.demo.zyy.pattern.observer.definition;

/**
 * 观察者测试
 */
public class ObserverMain {

    public static void main(String[] args) {
        Subject subject = new ConcreteSubject();
        subject.registerObserver(new ConcreteObserverA());
        subject.registerObserver(new ConcreteObserverB());

        subject.notifyObservers();
    }
}
