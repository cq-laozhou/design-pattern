package com.demo.zyy.pattern.observer.weather.v1;

/**
 * 气象统计布告板
 */
public class StatisticsDisplayV1 implements DisplayV1 {
    @Override
    public void update(WeatherDTOV1 data) {
        System.out.println("气象统计布告板更新，新数据:"+data.toString());
    }
}
