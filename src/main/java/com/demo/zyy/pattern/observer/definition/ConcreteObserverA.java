package com.demo.zyy.pattern.observer.definition;

/**
 * 具体观察者A
 */
public class ConcreteObserverA implements Observer{
    @Override
    public void update(Event event) {
        System.out.println("具体观察者A接收到事件，更新......");
    }
}
