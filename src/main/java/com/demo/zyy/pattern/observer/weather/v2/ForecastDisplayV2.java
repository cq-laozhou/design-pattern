package com.demo.zyy.pattern.observer.weather.v2;

/**
 * 天气预报布告板
 */
public class ForecastDisplayV2 implements DisplayV2 {
    @Override
    public void update(WeatherDTOV2 data) {
        System.out.println("天气预报布告板更新，新数据:"+data.toString());
    }
}
