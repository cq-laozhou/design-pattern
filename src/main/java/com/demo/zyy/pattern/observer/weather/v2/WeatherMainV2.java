package com.demo.zyy.pattern.observer.weather.v2;

/**
 * V2版测试
 */
public class WeatherMainV2 {
    public static void main(String[] args) {
        WeatherDataV2 weatherData = new WeatherDataV2();

        //注册观察者
        weatherData.registerDisplay(new CurrentConditionsDisplayV2());
        weatherData.registerDisplay(new ForecastDisplayV2());
        weatherData.registerDisplay(new StatisticsDisplayV2());

        //气象数据发生变更
        weatherData.setTemperature(34.5f);
        weatherData.setHumidity(125.7f);
        weatherData.setPressure(32.4f);

        //调用我们实现的方法。通知观察者
        weatherData.measurementsChanged();
    }
}
