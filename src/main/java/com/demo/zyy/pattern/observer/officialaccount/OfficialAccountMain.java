package com.demo.zyy.pattern.observer.officialaccount;

/**
 * 公众号测试
 */
public class OfficialAccountMain {

    public static void main(String[] args) {
        OfficialAccount account = new OfficialAccount("设计模式");
        Observer zhangsan = new Follower("张三");
        account.registerObserver(zhangsan);

        account.registerObserver(new Follower("李四"));
        account.registerObserver(new Follower("王五"));

        System.out.println("推送新文章。。。");
        account.pushArticle("观察者模式");

        account.removeObserver(zhangsan);
        System.out.println("推送新文章。。。");
        account.pushArticle("策略模式");
    }
}
