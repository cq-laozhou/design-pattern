package com.demo.zyy.pattern.observer.weather.v1;

/**
 * 目前状况布告板
 */
public class CurrentConditionsDisplayV1 implements DisplayV1 {
    @Override
    public void update(WeatherDTOV1 data) {
        System.out.println("目标状况布告板更新，新数据:"+data.toString());
    }
}
