package com.demo.zyy.pattern.observer.officialaccount;

import java.util.ArrayList;
import java.util.List;

/**
 * 公众号（具体主题）
 */
public class OfficialAccount implements Subject {
    private String name;
    private List<Observer> observers;
    private String lastArticle;


    public OfficialAccount(String name) {
        this.name = name;
        observers = new ArrayList<>();
    }

    @Override
    public void registerObserver(Observer observer) {
        observers.add(observer);
        System.out.println(name + "公众号新增粉丝...");
    }

    @Override
    public void removeObserver(Observer observer) {
        observers.remove(observer);
        System.out.println(name + "公众号掉粉...");
    }

    @Override
    public void notifyObservers() {
        Event event = new Event(name, lastArticle);
        observers.forEach(observer -> observer.update(event));
    }

    public void pushArticle(String articleName){
        this.lastArticle = articleName;
        notifyObservers();
    }
}
