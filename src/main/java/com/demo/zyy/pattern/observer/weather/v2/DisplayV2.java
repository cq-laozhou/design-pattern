package com.demo.zyy.pattern.observer.weather.v2;

/**
 * 布告板抽象（就是观察者抽象，有一个更新自己状态的方法)
 */
public interface DisplayV2 {

    /**
     * 更新布告板
     * @param data 气象数据
     */
    void update(WeatherDTOV2 data);
}
