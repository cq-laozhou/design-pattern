package com.demo.zyy.pattern.observer.definition;

/**
 * 抽象观察者
 */
public interface Observer {
    /**
     * 更新方法
     * @param event 事件信息
     */
    void update(Event event);
}
