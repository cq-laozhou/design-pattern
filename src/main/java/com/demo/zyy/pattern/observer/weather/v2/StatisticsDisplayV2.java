package com.demo.zyy.pattern.observer.weather.v2;

/**
 * 气象统计布告板
 */
public class StatisticsDisplayV2 implements DisplayV2 {
    @Override
    public void update(WeatherDTOV2 data) {
        System.out.println("气象统计布告板更新，新数据:"+data.toString());
    }
}
