package com.demo.zyy.pattern.observer.weather.v1;

/**
 * 布告板抽象
 */
public interface DisplayV1 {

    /**
     * 更新布告板
     * @param data 气象数据
     */
    void update(WeatherDTOV1 data);
}
