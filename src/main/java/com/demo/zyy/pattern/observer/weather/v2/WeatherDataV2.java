package com.demo.zyy.pattern.observer.weather.v2;

import java.util.ArrayList;
import java.util.List;

/**
 * 天气数据
 *  由气象站提供的类
 */
public class WeatherDataV2 implements Subject{

    private float temperature;  //温度
    private float humidity;     //湿度
    private float pressure;     //气压

    private List<DisplayV2> displays = new ArrayList<>(5); //布告板列表（观察者列表）


    /**
     * 当新的气象数据准备妥当时，会调用这个方法
     * 此时就需要我们来更新布告板信息。
     */
    public void measurementsChanged(){
        //这儿就是我们需要去实现的代码。
        notifyDisplay();
    }


    @Override
    public void registerDisplay(DisplayV2 display) {
        displays.add(display);
    }

    @Override
    public void removeDisplay(DisplayV2 display) {
        displays.remove(display);
    }

    @Override
    public void notifyDisplay() {
        WeatherDTOV2 weatherDTO =
                new WeatherDTOV2(getTemperature(), getHumidity(), getPressure());

        displays.forEach(display -> display.update(weatherDTO));
    }




    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    public float getHumidity() {
        return humidity;
    }

    public void setHumidity(float humidity) {
        this.humidity = humidity;
    }

    public float getPressure() {
        return pressure;
    }

    public void setPressure(float pressure) {
        this.pressure = pressure;
    }


}
