package com.demo.zyy.pattern.observer.weather.v1;

/**
 * 天气数据
 *  有气象站提供的类
 */
public class WeatherDataV1 {

    private float temperature;  //温度
    private float humidity;     //湿度
    private float pressure;     //气压

    /**
     * 当新的气象数据准备妥当时，会调用这个方法
     * 此时就需要我们来更新布告板信息。
     */
    public void measurementsChanged(){
        //这儿就是我们需要去实现的代码。
        WeatherDTOV1 weatherDTOV1 =
                new WeatherDTOV1(getTemperature(), getHumidity(), getPressure());

        CurrentConditionsDisplayV1 currentConditionsDisplay = new CurrentConditionsDisplayV1();
        currentConditionsDisplay.update(weatherDTOV1);

        StatisticsDisplayV1 statisticsDisplay = new StatisticsDisplayV1();
        statisticsDisplay.update(weatherDTOV1);

        ForecastDisplayV1 forecastDisplay = new ForecastDisplayV1();
        forecastDisplay.update(weatherDTOV1);
    }


    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    public float getHumidity() {
        return humidity;
    }

    public void setHumidity(float humidity) {
        this.humidity = humidity;
    }

    public float getPressure() {
        return pressure;
    }

    public void setPressure(float pressure) {
        this.pressure = pressure;
    }

}
