package com.demo.zyy.pattern.observer.weather.v1;

/**
 * 气象数据封装
 */
public class WeatherDTOV1 {
    private float temperature;  //温度
    private float humidity;     //湿度
    private float pressure;     //气压

    public WeatherDTOV1(float temperature, float humidity, float pressure) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.pressure = pressure;
    }

    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    public float getHumidity() {
        return humidity;
    }

    public void setHumidity(float humidity) {
        this.humidity = humidity;
    }

    public float getPressure() {
        return pressure;
    }

    public void setPressure(float pressure) {
        this.pressure = pressure;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("WeatherDTOV1{");
        sb.append("temperature=").append(temperature);
        sb.append(", humidity=").append(humidity);
        sb.append(", pressure=").append(pressure);
        sb.append('}');
        return sb.toString();
    }
}
