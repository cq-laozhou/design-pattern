package com.demo.zyy.pattern.observer.officialaccount;

/**
 * 事件（封装数据）
 */
public class Event {

    private String officialAccountName;
    private String articleName;

    public Event(String officialAccountName, String articleName) {
        this.officialAccountName = officialAccountName;
        this.articleName = articleName;
    }

    public String getArticleName() {
        return articleName;
    }

    public void setArticleName(String articleName) {
        this.articleName = articleName;
    }

    public String getOfficialAccountName() {
        return officialAccountName;
    }

    public void setOfficialAccountName(String officialAccountName) {
        this.officialAccountName = officialAccountName;
    }
}
