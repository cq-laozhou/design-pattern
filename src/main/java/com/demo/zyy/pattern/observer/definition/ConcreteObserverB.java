package com.demo.zyy.pattern.observer.definition;

/**
 * 具体观察者B
 */
public class ConcreteObserverB implements Observer{
    @Override
    public void update(Event event) {
        System.out.println("具体观察者B接收到事件，更新......");
    }
}
