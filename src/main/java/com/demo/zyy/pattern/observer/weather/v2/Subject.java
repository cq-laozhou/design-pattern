package com.demo.zyy.pattern.observer.weather.v2;

/**
 * 主题抽象
 */
public interface Subject {

    /**
     * 注册观察者
     * @param display
     */
    void registerDisplay(DisplayV2 display);

    /**
     * 移除观察者
     * @param display
     */
    void removeDisplay(DisplayV2 display);

    /**
     * 通知观察者
     */
    void notifyDisplay();
}
