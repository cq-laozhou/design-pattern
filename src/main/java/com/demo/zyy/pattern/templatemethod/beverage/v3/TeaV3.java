package com.demo.zyy.pattern.templatemethod.beverage.v3;

/**
 * 茶
 */
public class TeaV3 extends BaseBeverageV3{

    protected void brew() {
        System.out.println("用沸水冲泡茶");
    }

    protected void addCondiments() {
        System.out.println("加柠檬");
    }
}
