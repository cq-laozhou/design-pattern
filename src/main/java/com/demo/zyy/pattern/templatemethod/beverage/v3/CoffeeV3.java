package com.demo.zyy.pattern.templatemethod.beverage.v3;

/**
 * 咖啡
 */
public class CoffeeV3 extends BaseBeverageV3{

    @Override
    protected void brew() {
        System.out.println("用沸水冲泡咖啡");
    }

    @Override
    protected void addCondiments() {
        System.out.println("加糖和加奶");
    }
}
