package com.demo.zyy.pattern.templatemethod.beverage.v2;

/**
 * 饮料基类
 */
public abstract class BaseBeverageV2 {

    protected void boilWater() {
        System.out.println("把水煮沸");
    }
    protected void pourInCup() {
        System.out.println("到入杯子");
    }

}
