package com.demo.zyy.pattern.templatemethod.beverage.v4;

/**
 * 测试
 */
public class BeverageMainV4 {
    public static void main(String[] args) {
        CoffeeV4 coffee = new CoffeeV4();
        coffee.prepareRecipe();

        System.out.println();
        TeaV4 tea = new TeaV4();
        tea.prepareRecipe();
    }
}
