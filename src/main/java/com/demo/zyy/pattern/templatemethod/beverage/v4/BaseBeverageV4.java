package com.demo.zyy.pattern.templatemethod.beverage.v4;

/**
 * 饮料基类
 */
public abstract class BaseBeverageV4 {
    /**
     * 模板方法
     */
    public final void  prepareRecipe(){
        boilWater(); //把水煮沸
        brew(); //用沸水冲泡
        pourInCup();  //到入杯子
        if(wantsCondiments()) {
            addCondiments();  //加调料
        }
    }

    /**
     * 子类可以选择是否覆盖该方法，控制模板方法中是否要执行加调料步骤
     * @return
     */
    protected boolean wantsCondiments() {
        return true;
    }

    protected abstract void brew();

    protected abstract void addCondiments();

    protected final void boilWater() {
        System.out.println("把水煮沸");
    }
    protected final void pourInCup() {
        System.out.println("到入杯子");
    }

}
