package com.demo.zyy.pattern.templatemethod.beverage.v1;

/**
 * 茶
 */
public class TeaV1 {

    public void  prepareRecipe(){
        boilWater(); //把水煮沸
        steepTeaBag(); //用沸水冲泡茶
        pourInCup();  //把茶到入杯子
        addLemon();  //加柠檬
    }

    private void boilWater() {
        System.out.println("把水煮沸");
    }

    private void steepTeaBag() {
        System.out.println("用沸水冲泡茶");
    }

    private void pourInCup() {
        System.out.println("到入杯子");
    }

    private void addLemon() {
        System.out.println("加柠檬");
    }

}
