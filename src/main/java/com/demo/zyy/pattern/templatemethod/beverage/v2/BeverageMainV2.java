package com.demo.zyy.pattern.templatemethod.beverage.v2;

/**
 * 测试
 */
public class BeverageMainV2 {
    public static void main(String[] args) {
        CoffeeV2 coffee = new CoffeeV2();
        coffee.prepareRecipe();

        System.out.println();
        TeaV2 tea = new TeaV2();
        tea.prepareRecipe();
    }
}
