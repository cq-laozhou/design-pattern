package com.demo.zyy.pattern.templatemethod.beverage.v4;

/**
 * 咖啡
 */
public class CoffeeV4 extends BaseBeverageV4{

    @Override
    protected void brew() {
        System.out.println("用沸水冲泡咖啡");
    }

    @Override
    protected void addCondiments() {
        System.out.println("加糖和加奶");
    }
}
