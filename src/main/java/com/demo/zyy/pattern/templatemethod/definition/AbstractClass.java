package com.demo.zyy.pattern.templatemethod.definition;

/**
 * 包含了模板方法的抽象类
 */
public abstract class AbstractClass {

    /**
     * final的模板方法
     */
    public final void templateMethod(){
        primitiveOperation1(); //原语操作1
        primitiveOperation2(); //原语操作2
        concreteOperation();  //具体操作
    }

    protected abstract void primitiveOperation1();

    protected abstract void primitiveOperation2();

    private void concreteOperation() {
        System.out.println("抽象类中，具体的操作。。。");
    }

}
