package com.demo.zyy.pattern.templatemethod.dataparser;

/**
 * 测试
 */
public class DataParserMain {
    public static void main(String[] args) {
        CSVDataParser csvDataParser=new CSVDataParser();
        csvDataParser.parseDataAndGenerateOutput();
        System.out.println("**********************");
        DatabaseDataParser databaseDataParser=new DatabaseDataParser();
        databaseDataParser.parseDataAndGenerateOutput();
    }
}
