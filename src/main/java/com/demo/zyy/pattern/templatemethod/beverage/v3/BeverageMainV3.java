package com.demo.zyy.pattern.templatemethod.beverage.v3;

/**
 * 测试
 */
public class BeverageMainV3 {
    public static void main(String[] args) {
        CoffeeV3 coffee = new CoffeeV3();
        coffee.prepareRecipe();

        System.out.println();
        TeaV3 tea = new TeaV3();
        tea.prepareRecipe();
    }
}
