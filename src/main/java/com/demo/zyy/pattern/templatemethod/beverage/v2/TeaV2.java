package com.demo.zyy.pattern.templatemethod.beverage.v2;

/**
 * 茶
 */
public class TeaV2 extends BaseBeverageV2{

    public void  prepareRecipe(){
        boilWater(); //把水煮沸
        steepTeaBag(); //用沸水冲泡茶
        pourInCup();  //把茶到入杯子
        addLemon();  //加柠檬
    }

    private void steepTeaBag() {
        System.out.println("用沸水冲泡茶");
    }

    private void addLemon() {
        System.out.println("加柠檬");
    }

}
