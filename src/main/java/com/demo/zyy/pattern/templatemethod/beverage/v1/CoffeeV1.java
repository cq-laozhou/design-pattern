package com.demo.zyy.pattern.templatemethod.beverage.v1;

/**
 * 咖啡
 */
public class CoffeeV1 {

    public void  prepareRecipe(){
        boilWater(); //把水煮沸
        brewCoffeeGrinds(); //用沸水冲泡咖啡
        pourInCup();  //把咖啡到入杯子
        addSugarAndMilk();  //加糖和加奶
    }

    private void boilWater() {
        System.out.println("把水煮沸");
    }

    private void brewCoffeeGrinds() {
        System.out.println("用沸水冲泡咖啡");
    }

    private void pourInCup() {
        System.out.println("到入杯子");
    }

    private void addSugarAndMilk() {
        System.out.println("加糖和加奶");
    }
}
