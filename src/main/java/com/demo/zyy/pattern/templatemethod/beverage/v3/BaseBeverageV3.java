package com.demo.zyy.pattern.templatemethod.beverage.v3;

/**
 * 饮料基类
 */
public abstract class BaseBeverageV3 {
    /**
     * 模板方法
     */
    public final void  prepareRecipe(){
        boilWater(); //把水煮沸
        brew(); //用沸水冲泡
        pourInCup();  //到入杯子
        addCondiments();  //加调料
    }

    protected abstract void brew();

    protected abstract void addCondiments();

    protected void boilWater() {
        System.out.println("把水煮沸");
    }
    protected void pourInCup() {
        System.out.println("到入杯子");
    }

}
