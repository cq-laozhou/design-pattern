package com.demo.zyy.pattern.templatemethod.beverage.v4;

/**
 * 茶
 */
public class TeaV4 extends BaseBeverageV4{
    @Override
    protected boolean wantsCondiments() {
        return false;
    }

    protected void brew() {
        System.out.println("用沸水冲泡茶");
    }

    protected void addCondiments() {
        System.out.println("加柠檬");
    }
}
