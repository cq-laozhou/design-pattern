package com.demo.zyy.pattern.templatemethod.definition;

/**
 * 测试
 */
public class TemplateMethodMain {
    public static void main(String[] args) {
        AbstractClass abstractClass = new ConcreteClass();
        abstractClass.templateMethod();
    }
}
