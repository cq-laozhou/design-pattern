package com.demo.zyy.pattern.templatemethod.beverage.v1;

/**
 * 测试
 */
public class BeverageMainV1 {
    public static void main(String[] args) {
        CoffeeV1 coffee = new CoffeeV1();
        coffee.prepareRecipe();

        System.out.println();
        TeaV1 tea = new TeaV1();
        tea.prepareRecipe();
    }
}
