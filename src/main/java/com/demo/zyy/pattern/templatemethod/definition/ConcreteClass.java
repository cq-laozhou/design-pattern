package com.demo.zyy.pattern.templatemethod.definition;

/**
 * 具体类
 */
public class ConcreteClass extends AbstractClass {
    @Override
    protected void primitiveOperation1() {
        System.out.println("实现原语操作1....");
    }

    @Override
    protected void primitiveOperation2() {
        System.out.println("实现原语操作2....");
    }
}
