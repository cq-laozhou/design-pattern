package com.demo.zyy.pattern.templatemethod.beverage.v2;

/**
 * 咖啡
 */
public class CoffeeV2 extends BaseBeverageV2{

    public void  prepareRecipe(){
        boilWater(); //把水煮沸
        brewCoffeeGrinds(); //用沸水冲泡咖啡
        pourInCup();  //把咖啡到入杯子
        addSugarAndMilk();  //加糖和加奶
    }

    private void brewCoffeeGrinds() {
        System.out.println("用沸水冲泡咖啡");
    }

    private void addSugarAndMilk() {
        System.out.println("加糖和加奶");
    }
}
