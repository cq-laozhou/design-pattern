package com.demo.zyy.pattern.adapterfacade.electronicbusiness;

/**
 * 物流接口
 */
public interface ILogistics {
    void shipProducts(String productName, String shippingAddress);
}
