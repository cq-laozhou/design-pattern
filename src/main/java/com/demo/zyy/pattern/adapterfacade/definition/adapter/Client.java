package com.demo.zyy.pattern.adapterfacade.definition.adapter;

/**
 * 客户
 */
public class Client {
    private Target target;

    public Client(Target target) {
        this.target = target;
    }

    public void doSomething(){
        target.request();
    }
}
