package com.demo.zyy.pattern.adapterfacade.electronicbusiness;

/**
 * 库存接口
 */
public interface IInventory {
    void update(int productId);
}
