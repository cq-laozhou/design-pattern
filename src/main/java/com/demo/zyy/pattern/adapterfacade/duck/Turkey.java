package com.demo.zyy.pattern.adapterfacade.duck;

/**
 * 火鸡接口
 */
public interface Turkey {
    void gobble();
    void fly();
}
