package com.demo.zyy.pattern.adapterfacade.vendor;

/**
 * 原始的厂商API
 */
public interface OldVendorApi {

    void doSomething();

}
