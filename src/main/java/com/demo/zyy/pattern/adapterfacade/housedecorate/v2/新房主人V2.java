package com.demo.zyy.pattern.adapterfacade.housedecorate.v2;

/**
 * 亲力亲为的新房主人，要和所有人打交道，好累。
 */
public class 新房主人V2 {
    public void 装修房子(){
        装修公司 兄弟装修公司 = new 装修公司();
        兄弟装修公司.装修新房();
    }
}
