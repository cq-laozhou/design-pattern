package com.demo.zyy.pattern.adapterfacade.electronicbusiness;

/**
 * 支付接口
 */
public interface IPaymentGateway {
    boolean verifyCardDetails(String cardNo);
    boolean processPayment(String cardNo, float cost);
}
