package com.demo.zyy.pattern.adapterfacade.vendor.v2;

import com.demo.zyy.pattern.adapterfacade.vendor.OldVendorApi;

/**
 * 旧厂商接口适配器，实现旧的接口，实际功能调用到新的API上
 */
public class OldVendorApiAdapter implements OldVendorApi{
    private NewVendorApiV2 newVendorApi;

    public OldVendorApiAdapter(NewVendorApiV2 newVendorApi) {
        this.newVendorApi = newVendorApi;
    }

    @Override
    public void doSomething() {
        newVendorApi.doSomething2();
    }
}
