package com.demo.zyy.pattern.adapterfacade.electronicbusiness;

/**
 * 物流管理
 */
public class LogisticsManager implements ILogistics {
    @Override
    public void shipProducts(String productName, String shippingAddress) {
        String out = String.format(
                "Congratulations your product %s has been shipped at the following address: %s.",
                productName, shippingAddress);
        System.out.println(out);
    }
}
