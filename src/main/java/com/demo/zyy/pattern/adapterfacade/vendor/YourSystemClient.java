package com.demo.zyy.pattern.adapterfacade.vendor;

/**
 * 使用厂商类的客户端代码
 */
public class YourSystemClient {

    private OldVendorApi vendorAPI;

    public YourSystemClient(OldVendorApi vendorAPI) {
        this.vendorAPI = vendorAPI;
    }

    public void callVendorApi(){
        System.out.println("调用厂商类api....");
        vendorAPI.doSomething();
    }
}
