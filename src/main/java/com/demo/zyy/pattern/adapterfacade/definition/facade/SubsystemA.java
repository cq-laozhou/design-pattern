package com.demo.zyy.pattern.adapterfacade.definition.facade;

/**
 * 子系统A
 */
public class SubsystemA {
    public void doSpecial(){
        System.out.println("子系统A执行特殊的方法....");
    }
}
