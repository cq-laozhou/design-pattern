package com.demo.zyy.pattern.adapterfacade.definition.facade;

/**
 * 使用门面的客户
 */
public class Client {
    public void doSomeThing(){
        SystemFacade systemFacade = new SystemFacade();
        systemFacade.doSomeSpecialThing();
    }

    public static void main(String[] args) {
        new Client().doSomeThing();
    }
}


