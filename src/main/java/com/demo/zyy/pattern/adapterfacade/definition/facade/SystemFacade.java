package com.demo.zyy.pattern.adapterfacade.definition.facade;

/**
 * 门面，提供简单干净的对外接口
 */
public class SystemFacade {
    public void doSomeSpecialThing(){
        SubsystemA subsystemA = new SubsystemA();
        subsystemA.doSpecial();
        SubsystemB subsystemB = new SubsystemB();
        subsystemB.doSpecial();
        SubsystemC subsystemC = new SubsystemC();
        subsystemC.doSpecial();
    }
}
