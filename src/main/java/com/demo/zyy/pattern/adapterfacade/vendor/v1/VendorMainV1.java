package com.demo.zyy.pattern.adapterfacade.vendor.v1;

import com.demo.zyy.pattern.adapterfacade.vendor.YourSystemClient;

/**
 * 测试
 */
public class VendorMainV1 {
    public static void main(String[] args) {
        YourSystemClient systemClient = new YourSystemClient(new OldVendorApiImplV1());
        systemClient.callVendorApi();
    }
}
