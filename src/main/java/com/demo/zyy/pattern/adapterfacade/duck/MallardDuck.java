package com.demo.zyy.pattern.adapterfacade.duck;

/**
 * 绿头鸭
 */
public class MallardDuck implements Duck {
    @Override
    public void quack() {
        System.out.println("呱呱呱的叫");
    }

    @Override
    public void fly() {
        System.out.println("飞30米...");
    }
}
