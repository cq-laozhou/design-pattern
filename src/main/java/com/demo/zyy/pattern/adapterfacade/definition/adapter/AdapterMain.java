package com.demo.zyy.pattern.adapterfacade.definition.adapter;

/**
 * 测试
 */
public class AdapterMain {
    public static void main(String[] args) {
        Adapter adapter = new Adapter(new Adaptee());
        Client client = new Client(adapter);
        client.doSomething();
    }
}
