package com.demo.zyy.pattern.adapterfacade.definition.adapter;

/**
 * 目标接口
 */
public interface Target {
    void request();
}
