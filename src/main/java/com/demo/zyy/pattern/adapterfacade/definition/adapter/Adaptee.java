package com.demo.zyy.pattern.adapterfacade.definition.adapter;

/**
 * 被适配者
 */
public class Adaptee {
    public void specialRequest(){
        System.out.println("被适配者具体工作....");
    }
}
