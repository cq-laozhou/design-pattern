package com.demo.zyy.pattern.adapterfacade.electronicbusiness;

/**
 * 订单校验接口
 */
public interface IOrderVerify {
    boolean verifyShippingAddress(int pincode);
}
