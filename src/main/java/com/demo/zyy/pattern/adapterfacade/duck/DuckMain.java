package com.demo.zyy.pattern.adapterfacade.duck;

/**
 * 测试
 */
public class DuckMain {

    public static void main(String[] args) {
        Duck duck = new MallardDuck();
        duck.quack();
        duck.fly();
        System.out.println();

        duck = new TurkeyAdapter(new WildTurkey());
        duck.quack();
        duck.fly();
    }
}
