package com.demo.zyy.pattern.adapterfacade.vendor.v1;

import com.demo.zyy.pattern.adapterfacade.vendor.OldVendorApi;

/**
 * 旧厂商实现
 */
public class OldVendorApiImplV1 implements OldVendorApi {
    @Override
    public void doSomething() {
        System.out.println("执行旧厂商代码....");
    }
}
