package com.demo.zyy.pattern.adapterfacade.electronicbusiness;

/**
 * 订单检验管理
 */
public class OrderVerificationManager implements IOrderVerify {
    @Override
    public boolean verifyShippingAddress(int pincode) {
        System.out.println(
                "The product can be shipped to the pincode "
                        + pincode);
        return true;
    }
}
