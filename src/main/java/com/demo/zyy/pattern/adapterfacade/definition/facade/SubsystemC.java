package com.demo.zyy.pattern.adapterfacade.definition.facade;

/**
 * 子系统C
 */
public class SubsystemC {
    public void doSpecial(){
        System.out.println("子系统C执行特殊的方法....");
    }
}
