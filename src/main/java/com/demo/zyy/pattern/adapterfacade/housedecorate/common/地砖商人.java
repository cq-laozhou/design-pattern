package com.demo.zyy.pattern.adapterfacade.housedecorate.common;

/**
 * 地砖商人
 */
public class 地砖商人 {
    public void 购买地砖(){
        System.out.println("从地砖商人购买地砖");
    }
}
