package com.demo.zyy.pattern.adapterfacade.housedecorate.v2;

import com.demo.zyy.pattern.adapterfacade.housedecorate.common.地砖商人;
import com.demo.zyy.pattern.adapterfacade.housedecorate.common.木工;
import com.demo.zyy.pattern.adapterfacade.housedecorate.common.水泥商人;
import com.demo.zyy.pattern.adapterfacade.housedecorate.common.泥水工;
import com.demo.zyy.pattern.adapterfacade.housedecorate.common.电工;
import com.demo.zyy.pattern.adapterfacade.housedecorate.common.石材商人;

/**
 * 代替装修业主和各个角色打交道，脏活累活都有我来做吧。
 */
public class 装修公司 {
    public void 装修新房(){
        地砖商人 地砖商人 = new 地砖商人();
        地砖商人.购买地砖();

        水泥商人 水泥商人 = new 水泥商人();
        水泥商人.购买水泥();

        石材商人 石材商人 = new 石材商人();
        石材商人.购买石材();

        泥水工 泥水工 = new 泥水工();
        泥水工.铺地砖();

        木工 木工 = new 木工();
        木工.做柜子();

        电工 电工 = new 电工();
        电工.铺电线();
    }
}
