package com.demo.zyy.pattern.adapterfacade.vendor.v2;

/**
 * 新厂商实现
 */
public class NewVendorApiImplV2 implements NewVendorApiV2 {
    @Override
    public void doSomething2() {
        System.out.println("执行新的厂商代码....");
    }
}
