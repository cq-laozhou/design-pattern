package com.demo.zyy.pattern.adapterfacade.definition.adapter;

/**
 * 适配器，实现目标接口，将请求转发给被适配者
 */
public class Adapter implements Target {
    private Adaptee adaptee;

    public Adapter(Adaptee adaptee) {
        this.adaptee = adaptee;
    }

    @Override
    public void request() {
        adaptee.specialRequest();
    }
}
