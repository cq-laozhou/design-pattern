package com.demo.zyy.pattern.adapterfacade.duck;

/**
 * 鸭子接口
 */
public interface Duck {
    void quack();
    void fly();
}


