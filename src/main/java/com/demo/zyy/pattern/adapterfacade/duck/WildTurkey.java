package com.demo.zyy.pattern.adapterfacade.duck;

/**
 * 野生火鸡
 */
public class WildTurkey implements Turkey{
    @Override
    public void gobble() {
        System.out.println("咕咕的叫");
    }

    @Override
    public void fly() {
        System.out.println("飞了10米");
    }
}
