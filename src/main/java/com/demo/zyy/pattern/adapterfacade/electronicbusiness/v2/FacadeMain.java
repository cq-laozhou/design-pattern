package com.demo.zyy.pattern.adapterfacade.electronicbusiness.v2;

import com.demo.zyy.pattern.adapterfacade.electronicbusiness.OrderDetails;

/**
 * 使用外观后的客户长这个样子
 */
public class FacadeMain {
    public static void main(String[] args) {
        // Creating the Order/Product details
        OrderDetails orderDetails = new OrderDetails("Java Design Pattern book",
                "Simplified book on design patterns in Java",
                500, 10, "Street No 1", "Educational Area", 1212,
                "8811123456");

        // Using Facade
        OnlineShoppingFacade facade = new OnlineShoppingFacade();
        facade.finalizeOrder(orderDetails);
    }
}
