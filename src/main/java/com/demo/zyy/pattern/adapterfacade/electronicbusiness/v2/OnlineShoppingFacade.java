package com.demo.zyy.pattern.adapterfacade.electronicbusiness.v2;

import com.demo.zyy.pattern.adapterfacade.electronicbusiness.CostManager;
import com.demo.zyy.pattern.adapterfacade.electronicbusiness.ICosting;
import com.demo.zyy.pattern.adapterfacade.electronicbusiness.IInventory;
import com.demo.zyy.pattern.adapterfacade.electronicbusiness.ILogistics;
import com.demo.zyy.pattern.adapterfacade.electronicbusiness.IOrderVerify;
import com.demo.zyy.pattern.adapterfacade.electronicbusiness.IPaymentGateway;
import com.demo.zyy.pattern.adapterfacade.electronicbusiness.InventoryManager;
import com.demo.zyy.pattern.adapterfacade.electronicbusiness.LogisticsManager;
import com.demo.zyy.pattern.adapterfacade.electronicbusiness.OrderDetails;
import com.demo.zyy.pattern.adapterfacade.electronicbusiness.OrderVerificationManager;
import com.demo.zyy.pattern.adapterfacade.electronicbusiness.PaymentGatewayManager;

/**
 * 购物外观
 */
public class OnlineShoppingFacade {
    IInventory inventory = new InventoryManager();
    IOrderVerify orderVerify = new OrderVerificationManager();
    ICosting costManager = new CostManager();
    IPaymentGateway paymentGateway = new PaymentGatewayManager();
    ILogistics logistics = new LogisticsManager();

    public void finalizeOrder(OrderDetails orderDetails) {
        inventory.update(orderDetails.getProductNo());
        orderVerify.verifyShippingAddress(orderDetails.getPinCode());
        orderDetails.setPrice(
                costManager.applyDiscount(
                        orderDetails.getPrice(),
                        orderDetails.getDiscountPercent()
                )
        );
        paymentGateway.verifyCardDetails(orderDetails.getCardNo());
        paymentGateway.processPayment(orderDetails.getCardNo(), orderDetails.getPrice());
        String shippingAddress = String.format("%s, %s - %d",
                orderDetails.getAddressLine1(),
                orderDetails.getAddressLine2(),
                orderDetails.getPinCode());
        logistics.shipProducts(orderDetails.getCardNo(), shippingAddress);
    }
}
