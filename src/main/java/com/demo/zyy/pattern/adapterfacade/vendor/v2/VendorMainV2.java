package com.demo.zyy.pattern.adapterfacade.vendor.v2;

import com.demo.zyy.pattern.adapterfacade.vendor.YourSystemClient;

/**
 * 测试
 */
public class VendorMainV2 {
    public static void main(String[] args) {
        OldVendorApiAdapter oldVendorApiAdapter = new OldVendorApiAdapter(new NewVendorApiImplV2());
        YourSystemClient systemClient = new YourSystemClient(oldVendorApiAdapter);
        systemClient.callVendorApi();
    }
}
