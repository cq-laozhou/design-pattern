package com.demo.zyy.pattern.adapterfacade.electronicbusiness;

/**
 * 费用计算接口
 */
public interface ICosting {
    float applyDiscount(float price, float discountPercent);
}
