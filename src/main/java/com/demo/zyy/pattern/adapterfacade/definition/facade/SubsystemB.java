package com.demo.zyy.pattern.adapterfacade.definition.facade;

/**
 * 子系统B
 */
public class SubsystemB {
    public void doSpecial(){
        System.out.println("子系统B执行特殊的方法....");
    }
}
