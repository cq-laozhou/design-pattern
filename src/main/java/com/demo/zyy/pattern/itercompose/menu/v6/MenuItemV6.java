package com.demo.zyy.pattern.itercompose.menu.v6;

import java.util.Iterator;

/**
 * 菜单项
 *  叶子
 */
public class MenuItemV6 extends MenuComponentV6 {
    private String name;        //菜单名称
    private String desc;        //菜单描述
    private boolean vegetarian; //素食标识
    private double price;       //价格

    public MenuItemV6(String name, String desc, boolean vegetarian, double price) {
        this.name = name;
        this.desc = desc;
        this.vegetarian = vegetarian;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }

    public boolean isVegetarian() {
        return vegetarian;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public void print() {
        System.out.print("  " + getName());
        if(isVegetarian()){
            System.out.print("(v)");
        }
        System.out.println(", " + getPrice());
        System.out.println("    -- " + getDesc());
    }

    @Override
    public Iterator<MenuComponentV6> createIterator() {
        return new NullIterator();
    }
}
