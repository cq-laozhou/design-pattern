package com.demo.zyy.pattern.itercompose.menu.v2;

import com.demo.zyy.pattern.itercompose.menu.common.MenuItem;

import java.util.ArrayList;

/**
 * 煎饼屋菜单迭代器实现
 */
public class PancakeHouseMenuIteratorV2 implements MenuIteratorV2 {
    private ArrayList<MenuItem> menuItems;
    private int position = 0;

    public PancakeHouseMenuIteratorV2(ArrayList<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }

    @Override
    public boolean hasNext() {
        if(position >= menuItems.size()){
            return false;
        }else {
            return true;
        }
    }

    @Override
    public MenuItem next() {
        MenuItem menuItem = menuItems.get(position);
        position++;
        return menuItem;
    }
}
