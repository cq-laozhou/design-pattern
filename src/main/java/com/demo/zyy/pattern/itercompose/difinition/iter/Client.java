package com.demo.zyy.pattern.itercompose.difinition.iter;

/**
 * 客户
 */
public class Client {

    private Aggregate aggregate;

    public Client(Aggregate aggregate) {
        this.aggregate = aggregate;
    }

    public void traverse(){
        Iter iter = aggregate.createIter();
        while (iter.hasNext()){
            System.out.println(iter.next().toString());
        }
    }

    public static void main(String[] args) {
        new Client(new ConcreteAggregate()).traverse();
    }
}
