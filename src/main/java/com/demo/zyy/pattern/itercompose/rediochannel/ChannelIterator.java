package com.demo.zyy.pattern.itercompose.rediochannel;

/**
 * 频道迭代器接口
 */
public interface ChannelIterator {
    public boolean hasNext();

    public Channel next();
}
