package com.demo.zyy.pattern.itercompose.menu.v4;

import com.demo.zyy.pattern.itercompose.menu.common.MenuItem;
import com.demo.zyy.pattern.itercompose.menu.v3.DinerMenuV3;
import com.demo.zyy.pattern.itercompose.menu.v3.PancakeHouseMenuV3;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 服务员
 */
public class WaitressV4 {

    private List<Iterable<MenuItem>> iterables;

    public WaitressV4() {
        this.iterables = new ArrayList<>();
    }

    public void addMenu(Iterable<MenuItem> iterable){
        iterables.add(iterable);
    }

    //报全部菜单
    public void printMenu(){
        System.out.println("菜单:");
        for (Iterable<MenuItem> iterable : iterables) {
            printMenu(iterable.iterator());
            System.out.println();
        }
    }

    private void printMenu(Iterator<MenuItem> menuItemIterator) {
        while(menuItemIterator.hasNext()){
            MenuItem menuItem = menuItemIterator.next();
            System.out.println(menuItem.getName() + "\t" + menuItem.getPrice() + "\t" + menuItem.getDesc());
        }
    }

    //只报早餐菜单
    public void printBreakfastMenu(){

    }

    //只报午餐菜单
    public void printLunchMenu(){}

    //只报素食菜单
    public void printVegetarianMenu(){}

    //指定的菜单项的名称，判断是否为素食
    public void isVegetarian(String name){}
}
