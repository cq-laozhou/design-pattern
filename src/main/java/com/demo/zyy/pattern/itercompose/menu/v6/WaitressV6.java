package com.demo.zyy.pattern.itercompose.menu.v6;

import com.demo.zyy.pattern.itercompose.menu.v5.MenuComponentV5;

import java.util.Iterator;

/**
 * 服务员
 */
public class WaitressV6 {

    private MenuComponentV6 rootMenu;

    public WaitressV6(MenuComponentV6 rootMenu) {
        this.rootMenu = rootMenu;
    }

    //报全部菜单
    public void printMenu(){
        System.out.println("菜单:");
        rootMenu.print();
    }

    //报素食菜单
    public void printVegetarianMenu(){
        System.out.println("素食菜单:");
        Iterator<MenuComponentV6> iterator = rootMenu.createIterator();
        while(iterator.hasNext()){
            MenuComponentV6 component = iterator.next();
            try {
                if(component.isVegetarian()){
                    component.print();
                }
            } catch (UnsupportedOperationException e) {
                //组合上的额打印异常忽略。
            }
        }
    }

}
