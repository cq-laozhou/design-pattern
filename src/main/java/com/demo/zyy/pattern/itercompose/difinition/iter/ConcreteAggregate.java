package com.demo.zyy.pattern.itercompose.difinition.iter;

import java.util.ArrayList;
import java.util.List;

/**
 * 具体的聚集对象
 */
public class ConcreteAggregate implements Aggregate{

    private List list = new ArrayList(); //简单起见，其内部使用list。

    public ConcreteAggregate() {
        for (int i = 0; i < 10; i++) {
            list.add("element" + i);
        }
    }

    @Override
    public Iter createIter() {
        return new ConcreteIter(list);
    }
}
