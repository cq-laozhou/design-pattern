package com.demo.zyy.pattern.itercompose.menu.v4;

import com.demo.zyy.pattern.itercompose.menu.common.MenuItem;

import java.util.HashMap;
import java.util.Iterator;

/**
 * 新增的咖啡菜单
 */
public class CafeMenuV4 implements Iterable<MenuItem> {

    private HashMap<String, MenuItem> menuItemMap = new HashMap<>();

    public CafeMenuV4() {
        addItem("简单的热咖啡", "原味咖啡，带感", true, 5.0d);
        addItem("加奶热咖啡", "热咖啡，家电奶，口感不错", false, 7.0d);
        addItem("加糖冰咖啡", "冰咖啡，可能放点糖要好点", true, 8.0d);
    }

    private void addItem(String name, String desc, boolean vegetarian, double price) {
        addItem(new MenuItem(name, desc, vegetarian, price));
    }

    private void addItem(MenuItem menuItem) {
        menuItemMap.put(menuItem.getName(), menuItem);
    }

    @Override
    public Iterator<MenuItem> iterator() {
        return menuItemMap.values().iterator();
    }
}
