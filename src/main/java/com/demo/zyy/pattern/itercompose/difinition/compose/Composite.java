package com.demo.zyy.pattern.itercompose.difinition.compose;

import java.util.ArrayList;
import java.util.List;

/**
 * 非叶子节点， 对象组合
 */
public class Composite implements Component {
    private List<Component> childs;

    private String name;

    public Composite(String name) {
        this.name = name;
        this.childs = new ArrayList<>();
    }

    @Override
    public void operation() {
        System.out.println("-----在组合节点"+name+"上的操作------");
        childs.forEach(child -> child.operation());
    }

    @Override
    public void add(Component component) {
        childs.add(component);
    }

    @Override
    public void remove(Component component) {
        childs.remove(component);
    }
}
