package com.demo.zyy.pattern.itercompose.menu.v6;

import java.util.Iterator;

/**
 * 菜单组件接口
 */
public abstract class MenuComponentV6 {

    public void add(MenuComponentV6 menuComponent){
        throw new UnsupportedOperationException();
    }

    public void remove(MenuComponentV6 menuComponent){
        throw new UnsupportedOperationException();
    }

    public MenuComponentV6 getChild(int i){
        throw new UnsupportedOperationException();
    }

    public String getName() {
        throw new UnsupportedOperationException();
    }

    public String getDesc() {
        throw new UnsupportedOperationException();
    }

    public boolean isVegetarian() {
        throw new UnsupportedOperationException();
    }

    public double getPrice() {
        throw new UnsupportedOperationException();
    }

    public void print(){
        throw new UnsupportedOperationException();
    }

    public abstract Iterator<MenuComponentV6> createIterator();
}
