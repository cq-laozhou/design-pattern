package com.demo.zyy.pattern.itercompose.organization;

/**
 * 组件接口 雇员类
 */
public interface Employee {
    public void add(Employee employee);
    public void remove(Employee employee);
    public Employee getChild(int i);
    public String getName();
    public double getSalary();
    public void print();
}
