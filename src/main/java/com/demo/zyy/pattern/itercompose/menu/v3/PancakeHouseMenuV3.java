package com.demo.zyy.pattern.itercompose.menu.v3;

import com.demo.zyy.pattern.itercompose.menu.common.MenuItem;
import com.demo.zyy.pattern.itercompose.menu.v2.MenuIteratorV2;
import com.demo.zyy.pattern.itercompose.menu.v2.PancakeHouseMenuIteratorV2;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * 煎饼屋菜单
 */
public class PancakeHouseMenuV3 implements Iterable<MenuItem>{
    private ArrayList<MenuItem> menuItems;

    public PancakeHouseMenuV3() {
        menuItems = new ArrayList<>();

        addItem("I型煎饼", "I型煎饼是纯煎饼，不加任何东西", true, 2.0d);
        addItem("II型煎饼", "II型煎饼，加一个鸡蛋", false, 3.0d);
        addItem("III型煎饼", "III型煎饼，加里脊肉，好吃", false, 4.0d);
        addItem("IIII型煎饼", "IIII型煎饼，又加鸡蛋又加里脊肉，再来点辣酱,爽", false, 5.0d);
    }

    private void addItem(String name, String desc, boolean vegetarian, double price) {
        addItem(new MenuItem(name, desc, vegetarian, price));
    }

    private void addItem(MenuItem menuItem) {
        menuItems.add(menuItem);
    }

    @Override
    public Iterator<MenuItem> iterator() {
        return menuItems.iterator();
    }
}
