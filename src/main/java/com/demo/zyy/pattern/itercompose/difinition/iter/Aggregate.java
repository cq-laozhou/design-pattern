package com.demo.zyy.pattern.itercompose.difinition.iter;

/**
 * 抽象的聚集对象
 */
public interface Aggregate {
    Iter createIter(); //获取迭代器对象
}
