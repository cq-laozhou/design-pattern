package com.demo.zyy.pattern.itercompose.menu.v4;

import com.demo.zyy.pattern.itercompose.menu.common.MenuItem;

import java.util.Iterator;

/**
 * 午餐菜单迭代器实现
 */
public class DinerMenuIteratorV4 implements Iterator<MenuItem> {
    private MenuItem[] menuItems;
    private int position = 0;

    public DinerMenuIteratorV4(MenuItem[] menuItems) {
        this.menuItems = menuItems;
    }

    @Override
    public boolean hasNext() {
        if(position > menuItems.length || menuItems[position] == null){
            return false;
        }else {
            return true;
        }
    }

    @Override
    public MenuItem next() {
        MenuItem menuItem = menuItems[position];
        position++;
        return menuItem;
    }
}
