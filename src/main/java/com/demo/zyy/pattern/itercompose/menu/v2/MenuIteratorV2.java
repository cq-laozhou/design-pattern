package com.demo.zyy.pattern.itercompose.menu.v2;

import com.demo.zyy.pattern.itercompose.menu.common.MenuItem;

/**
 * 菜单迭代器
 */
public interface MenuIteratorV2 {
    boolean hasNext();  //是否有下一个
    MenuItem next();    //取下一个
}
