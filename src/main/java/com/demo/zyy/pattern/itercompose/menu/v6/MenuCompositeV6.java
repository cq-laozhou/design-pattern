package com.demo.zyy.pattern.itercompose.menu.v6;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 菜单组合对象
 *   可包括菜单组件孩子
 */
public class MenuCompositeV6 extends MenuComponentV6 {
    private List<MenuComponentV6> childs = new ArrayList<>();
    private String name;
    private String desc;

    public MenuCompositeV6(String name, String desc) {
        this.name = name;
        this.desc = desc;
    }

    @Override
    public void add(MenuComponentV6 menuComponent) {
        childs.add(menuComponent);
    }

    @Override
    public void remove(MenuComponentV6 menuComponent) {
        childs.remove(menuComponent);
    }

    @Override
    public MenuComponentV6 getChild(int i) {
        return childs.get(i);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDesc() {
        return desc;
    }

    @Override
    public void print() {
        System.out.print("\n" + getName());
        System.out.println(", " + getDesc());
        System.out.println("------------------");

        for (MenuComponentV6 child : childs) {
            child.print();
        }
    }

    @Override
    public Iterator<MenuComponentV6> createIterator() {
        return new CompositeIterator(childs.iterator());
    }
}
