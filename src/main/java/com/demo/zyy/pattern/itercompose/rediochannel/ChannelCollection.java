package com.demo.zyy.pattern.itercompose.rediochannel;

/**
 * 频道集合接口
 */
public interface ChannelCollection {
    public void addChannel(Channel c);

    public void removeChannel(Channel c);

    public ChannelIterator iterator(ChannelTypeEnum type);
}
