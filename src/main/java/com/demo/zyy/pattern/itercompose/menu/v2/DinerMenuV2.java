package com.demo.zyy.pattern.itercompose.menu.v2;

import com.demo.zyy.pattern.itercompose.menu.common.MenuItem;

/**
 * 午餐菜单
 */
public class DinerMenuV2 {
    private static final int MAX_ITEMS = 6;
    private int numberOfItems = 0;
    private MenuItem[] menuItems;

    public DinerMenuV2() {
        menuItems = new MenuItem[6];

        addItem("红烧茄子套餐", "红烧茄子，好词又营养，还不贵", true, 12.0d);
        addItem("麻婆豆腐套餐", "麻婆豆腐，麻辣鲜香", true, 13.0d);
        addItem("酸菜鱼套餐", "可以喝汤的酸菜鱼，又酸又鲜还无刺，你值得拥有", false, 24.0d);
        addItem("红烧牛肉套餐", "选用定级牛肉，加入各种调料炖煮8个小时，入口即化，好吃到爆", false, 35.0d);
    }

    private void addItem(String name, String desc, boolean vegetarian, double price) {
        addItem(new MenuItem(name, desc, vegetarian, price));
    }

    private void addItem(MenuItem menuItem) {
        if(numberOfItems >= MAX_ITEMS){
            System.err.println("菜单已满，不能再往里添加菜单项了..");
        }else{
            menuItems[numberOfItems] = menuItem;
            numberOfItems++;
        }
    }

    public MenuIteratorV2 createIterator(){
        return new DinerMenuIteratorV2(menuItems);
    }
}
