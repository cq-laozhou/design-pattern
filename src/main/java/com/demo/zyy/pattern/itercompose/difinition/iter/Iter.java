package com.demo.zyy.pattern.itercompose.difinition.iter;

/**
 * 迭代器接口
 */
public interface Iter {
    boolean hasNext();
    Object next();
}
