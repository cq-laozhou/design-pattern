package com.demo.zyy.pattern.itercompose.menu.common;

/**
 * 菜单项
 */
public class MenuItem {
    private String name;        //菜单名称
    private String desc;        //菜单描述
    private boolean vegetarian; //素食标识
    private double price;       //价格

    public MenuItem(String name, String desc, boolean vegetarian, double price) {
        this.name = name;
        this.desc = desc;
        this.vegetarian = vegetarian;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }

    public boolean isVegetarian() {
        return vegetarian;
    }

    public double getPrice() {
        return price;
    }
}
