package com.demo.zyy.pattern.itercompose.rediochannel;

/**
 * 频道类型
 */
public enum  ChannelTypeEnum {
    ENGLISH, HINDI, FRENCH, ALL;
}
