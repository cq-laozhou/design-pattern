package com.demo.zyy.pattern.itercompose.menu.v5;

/**
 * 菜单组件接口
 */
public abstract class MenuComponentV5 {

    public void add(MenuComponentV5 menuComponent){
        throw new UnsupportedOperationException();
    }

    public void remove(MenuComponentV5 menuComponent){
        throw new UnsupportedOperationException();
    }

    public MenuComponentV5 getChild(int i){
        throw new UnsupportedOperationException();
    }

    public String getName() {
        throw new UnsupportedOperationException();
    }

    public String getDesc() {
        throw new UnsupportedOperationException();
    }

    public boolean isVegetarian() {
        throw new UnsupportedOperationException();
    }

    public double getPrice() {
        throw new UnsupportedOperationException();
    }

    public void print(){
        throw new UnsupportedOperationException();
    }

}
