package com.demo.zyy.pattern.itercompose.menu.v6;

/**
 * 测试
 */
public class MenuMainV6 {
    public static void main(String[] args) {
        MenuComponentV6 rootMenu = new MenuCompositeV6("所有菜单", "菜单树根");

        MenuComponentV6 pancakeHouseMenu = new MenuCompositeV6("煎饼屋菜单", "早餐");
        pancakeHouseMenu.add(new MenuItemV6("I型煎饼", "I型煎饼是纯煎饼，不加任何东西", true, 2.0d));
        pancakeHouseMenu.add(new MenuItemV6("II型煎饼", "II型煎饼，加一个鸡蛋", false, 3.0d));
        pancakeHouseMenu.add(new MenuItemV6("III型煎饼", "III型煎饼，加里脊肉，好吃", false, 4.0d));
        pancakeHouseMenu.add(new MenuItemV6("IIII型煎饼", "IIII型煎饼，又加鸡蛋又加里脊肉，再来点辣酱,爽", false, 5.0d));

        rootMenu.add(pancakeHouseMenu);

        MenuComponentV6 dinerMenu = new MenuCompositeV6("午餐菜单", "午餐");
        dinerMenu.add(new MenuItemV6("红烧茄子套餐", "红烧茄子，好词又营养，还不贵", true, 12.0d));
        dinerMenu.add(new MenuItemV6("麻婆豆腐套餐", "麻婆豆腐，麻辣鲜香", true, 13.0d));
        dinerMenu.add(new MenuItemV6("酸菜鱼套餐", "可以喝汤的酸菜鱼，又酸又鲜还无刺，你值得拥有", false, 24.0d));
        dinerMenu.add(new MenuItemV6("红烧牛肉套餐", "选用定级牛肉，加入各种调料炖煮8个小时，入口即化，好吃到爆", false, 35.0d));

        MenuComponentV6 dessertMenu = new MenuCompositeV6("甜点菜单", "甜点");
        dessertMenu.add(new MenuItemV6("苹果派", "苹果派冰淇淋..", true, 3.5d));
        dessertMenu.add(new MenuItemV6("蓝莓派", "蓝莓派冰淇淋..", true, 5.5d));

        dinerMenu.add(dessertMenu);

        rootMenu.add(dinerMenu);


        MenuComponentV6 cafeMenu = new MenuCompositeV6("咖啡菜单", "晚餐");
        cafeMenu.add(new MenuItemV6("简单的热咖啡", "原味咖啡，带感", true, 5.0d));
        cafeMenu.add(new MenuItemV6("加奶热咖啡", "热咖啡，家电奶，口感不错", false, 7.0d));
        cafeMenu.add(new MenuItemV6("加糖冰咖啡", "冰咖啡，可能放点糖要好点", true, 8.0d));

        rootMenu.add(cafeMenu);

        WaitressV6 waitress = new WaitressV6(rootMenu);
        waitress.printVegetarianMenu();
    }
}
