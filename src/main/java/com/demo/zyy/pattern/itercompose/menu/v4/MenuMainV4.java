package com.demo.zyy.pattern.itercompose.menu.v4;

/**
 * 测试
 */
public class MenuMainV4 {
    public static void main(String[] args) {
        PancakeHouseMenuV4 pancakeHouseMenu = new PancakeHouseMenuV4();
        DinerMenuV4 dinerMenu = new DinerMenuV4();

        CafeMenuV4 cafeMenu = new CafeMenuV4();

        WaitressV4 waitress = new WaitressV4();
        waitress.addMenu(pancakeHouseMenu);
        waitress.addMenu(dinerMenu);
        waitress.addMenu(cafeMenu);

        waitress.printMenu();
    }
}
