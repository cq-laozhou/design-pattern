package com.demo.zyy.pattern.itercompose.difinition.compose;

/**
 * 组合接口
 *  是叶子节点和组合节点的共同接口。
 */
public interface Component {

    void operation();  //

    void add(Component component);

    void remove(Component component);
}
