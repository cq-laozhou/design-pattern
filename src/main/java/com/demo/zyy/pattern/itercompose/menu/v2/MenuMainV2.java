package com.demo.zyy.pattern.itercompose.menu.v2;

/**
 * 测试
 */
public class MenuMainV2 {
    public static void main(String[] args) {
        PancakeHouseMenuV2 pancakeHouseMenu = new PancakeHouseMenuV2();
        DinerMenuV2 dinerMenu = new DinerMenuV2();
        WaitressV2 waitress = new WaitressV2(pancakeHouseMenu, dinerMenu);
        waitress.printMenu();
    }
}
