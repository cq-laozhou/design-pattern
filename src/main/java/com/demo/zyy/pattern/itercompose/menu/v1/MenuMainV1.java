package com.demo.zyy.pattern.itercompose.menu.v1;

/**
 * 测试
 */
public class MenuMainV1 {
    public static void main(String[] args) {
        PancakeHouseMenuV1 pancakeHouseMenu = new PancakeHouseMenuV1();
        DinerMenuV1 dinerMenu = new DinerMenuV1();
        WaitressV1 waitress = new WaitressV1(pancakeHouseMenu, dinerMenu);
        waitress.printMenu();
    }
}
