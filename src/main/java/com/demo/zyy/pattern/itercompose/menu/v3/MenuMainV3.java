package com.demo.zyy.pattern.itercompose.menu.v3;

/**
 * 测试
 */
public class MenuMainV3 {
    public static void main(String[] args) {
        PancakeHouseMenuV3 pancakeHouseMenu = new PancakeHouseMenuV3();
        DinerMenuV3 dinerMenu = new DinerMenuV3();
        WaitressV3 waitress = new WaitressV3(pancakeHouseMenu, dinerMenu);
        waitress.printMenu();
    }
}
