package com.demo.zyy.pattern.itercompose.menu.v6;

import java.util.Iterator;

/**
 * 空迭代器。
 */
public class NullIterator implements Iterator<MenuComponentV6> {
    @Override
    public boolean hasNext() {
        return false;
    }

    @Override
    public MenuComponentV6 next() {
        return null;
    }
}
