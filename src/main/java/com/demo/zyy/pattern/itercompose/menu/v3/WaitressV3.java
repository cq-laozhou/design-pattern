package com.demo.zyy.pattern.itercompose.menu.v3;

import com.demo.zyy.pattern.itercompose.menu.common.MenuItem;
import com.demo.zyy.pattern.itercompose.menu.v2.DinerMenuV2;
import com.demo.zyy.pattern.itercompose.menu.v2.MenuIteratorV2;
import com.demo.zyy.pattern.itercompose.menu.v2.PancakeHouseMenuV2;

import java.util.Iterator;

/**
 * 服务员
 */
public class WaitressV3 {

    private Iterable<MenuItem> pancakeHouseMenu;
    private Iterable<MenuItem> dinerMenu;

    public WaitressV3(PancakeHouseMenuV3 pancakeHouseMenu, DinerMenuV3 dinerMenu) {
        this.pancakeHouseMenu = pancakeHouseMenu;
        this.dinerMenu = dinerMenu;
    }
    //报全部菜单
    public void printMenu(){
        System.out.println("早餐菜单:");
        Iterator<MenuItem> pancakeHouseMenuIterator = pancakeHouseMenu.iterator();
        printMenu(pancakeHouseMenuIterator);
        System.out.println("午餐菜单:");
        Iterator<MenuItem> dinerMenuIterator = dinerMenu.iterator();
        printMenu(dinerMenuIterator);
    }

    private void printMenu(Iterator<MenuItem> menuItemIterator) {
        while(menuItemIterator.hasNext()){
            MenuItem menuItem = menuItemIterator.next();
            System.out.println(menuItem.getName() + "\t" + menuItem.getPrice() + "\t" + menuItem.getDesc());
        }
    }

    //只报早餐菜单
    public void printBreakfastMenu(){

    }

    //只报午餐菜单
    public void printLunchMenu(){}

    //只报素食菜单
    public void printVegetarianMenu(){}

    //指定的菜单项的名称，判断是否为素食
    public void isVegetarian(String name){}
}
