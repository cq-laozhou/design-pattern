package com.demo.zyy.pattern.itercompose.difinition.compose;

/**
 * 客户
 */
public class Client {

    public static void main(String[] args) {
        Component root = new Composite("根节点");

        Component compositeA = new Composite("组合节点A");
        Component compositeB = new Composite("组合节点B");

        compositeA.add(new Leaf("LeafA-1"));
        compositeA.add(new Leaf("LeafA-2"));

        compositeB.add(new Leaf("LeafB-1"));
        compositeB.add(new Leaf("LeafB-2"));

        root.add(compositeA);
        root.add(compositeB);

        root.operation();
    }
}
