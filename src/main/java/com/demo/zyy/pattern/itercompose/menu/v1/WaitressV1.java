package com.demo.zyy.pattern.itercompose.menu.v1;

import com.demo.zyy.pattern.itercompose.menu.common.MenuItem;

import java.util.ArrayList;

/**
 * 服务员
 */
public class WaitressV1 {

    private PancakeHouseMenuV1 pancakeHouseMenu;
    private DinerMenuV1 dinerMenu;

    public WaitressV1(PancakeHouseMenuV1 pancakeHouseMenu, DinerMenuV1 dinerMenu) {
        this.pancakeHouseMenu = pancakeHouseMenu;
        this.dinerMenu = dinerMenu;
    }
    //报全部菜单
    public void printMenu(){
        System.out.println("早餐菜单:");
        ArrayList<MenuItem> pancakeHouseMenuItems = pancakeHouseMenu.getMenuItems();
        for (int i = 0; i < pancakeHouseMenuItems.size(); i++) {
            MenuItem menuItem = pancakeHouseMenuItems.get(i);
            System.out.println(menuItem.getName() + "\t" + menuItem.getPrice() + "\t" + menuItem.getDesc());
        }
        System.out.println("午餐菜单:");
        MenuItem[] dinerMenuItems = dinerMenu.getMenuItems();
        for (int i = 0; i < dinerMenuItems.length; i++) {
            MenuItem menuItem = dinerMenuItems[i];
            if(null == menuItem){
                continue;
            }
            System.out.println(menuItem.getName() + "\t" + menuItem.getPrice() + "\t" + menuItem.getDesc());
        }
    }

    //只报早餐菜单
    public void printBreakfastMenu(){}

    //只报午餐菜单
    public void printLunchMenu(){}

    //只报素食菜单
    public void printVegetarianMenu(){}

    //指定的菜单项的名称，判断是否为素食
    public void isVegetarian(String name){}
}
