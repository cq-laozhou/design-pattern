package com.demo.zyy.pattern.itercompose.menu.v2;

import com.demo.zyy.pattern.itercompose.menu.common.MenuItem;

/**
 * 午餐菜单迭代器实现
 */
public class DinerMenuIteratorV2 implements MenuIteratorV2 {
    private MenuItem[] menuItems;
    private int position = 0;

    public DinerMenuIteratorV2(MenuItem[] menuItems) {
        this.menuItems = menuItems;
    }

    @Override
    public boolean hasNext() {
        if(position > menuItems.length || menuItems[position] == null){
            return false;
        }else {
            return true;
        }
    }

    @Override
    public MenuItem next() {
        MenuItem menuItem = menuItems[position];
        position++;
        return menuItem;
    }
}
