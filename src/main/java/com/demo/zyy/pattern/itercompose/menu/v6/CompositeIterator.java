package com.demo.zyy.pattern.itercompose.menu.v6;

import java.util.Iterator;
import java.util.Stack;

/**
 * 菜单组合对象迭代器。
 */
public class CompositeIterator implements Iterator<MenuComponentV6> {
    private Stack<Iterator<MenuComponentV6>> stack;

    public CompositeIterator(Iterator<MenuComponentV6> iterator) {
        stack = new Stack<>();
        stack.push(iterator);
    }

    @Override
    public boolean hasNext() {
        if(stack.isEmpty()){
            return false;
        }else {
            Iterator<MenuComponentV6> iterator = stack.peek();
            if(!iterator.hasNext()){
                stack.pop();
                return hasNext();
            }else{
                return true;
            }
        }
    }

    @Override
    public MenuComponentV6 next() {
        if(hasNext()){
            Iterator<MenuComponentV6> iterator = stack.peek();
            MenuComponentV6 component = iterator.next();
            if(component instanceof MenuCompositeV6){
                stack.push(component.createIterator());
            }
            return component;
        }else{
            return null;
        }
    }
}
