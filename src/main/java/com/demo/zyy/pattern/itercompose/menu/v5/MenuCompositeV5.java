package com.demo.zyy.pattern.itercompose.menu.v5;

import java.util.ArrayList;
import java.util.List;

/**
 * 菜单组合对象
 *   可包括菜单组件孩子
 */
public class MenuCompositeV5 extends MenuComponentV5 {
    private List<MenuComponentV5> childs = new ArrayList<>();
    private String name;
    private String desc;

    public MenuCompositeV5(String name, String desc) {
        this.name = name;
        this.desc = desc;
    }

    @Override
    public void add(MenuComponentV5 menuComponent) {
        childs.add(menuComponent);
    }

    @Override
    public void remove(MenuComponentV5 menuComponent) {
        childs.remove(menuComponent);
    }

    @Override
    public MenuComponentV5 getChild(int i) {
        return childs.get(i);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDesc() {
        return desc;
    }

    @Override
    public void print() {
        System.out.print("\n" + getName());
        System.out.println(", " + getDesc());
        System.out.println("------------------");

        for (MenuComponentV5 child : childs) {
            child.print();
        }
    }
}
