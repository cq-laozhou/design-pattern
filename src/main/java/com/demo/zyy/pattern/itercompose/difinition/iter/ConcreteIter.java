package com.demo.zyy.pattern.itercompose.difinition.iter;

import java.util.Iterator;
import java.util.List;

/**
 * 具体迭代器实现
 *  它内部会封装不同的数据结构
 */
public class ConcreteIter implements Iter {
    private List list;
    private Iterator insideIter;  //简单起见，使用List自生的迭代器实现。

    public ConcreteIter(List list) {
        this.list = list;
        insideIter = this.list.iterator();
    }

    @Override
    public boolean hasNext() {
        return insideIter.hasNext();
    }

    @Override
    public Object next() {
        return insideIter.next();
    }
}
