package com.demo.zyy.pattern.itercompose.menu.v2;

import com.demo.zyy.pattern.itercompose.menu.common.MenuItem;
import com.demo.zyy.pattern.itercompose.menu.v1.DinerMenuV1;
import com.demo.zyy.pattern.itercompose.menu.v1.PancakeHouseMenuV1;

import java.util.ArrayList;

/**
 * 服务员
 */
public class WaitressV2 {

    private PancakeHouseMenuV2 pancakeHouseMenu;
    private DinerMenuV2 dinerMenu;

    public WaitressV2(PancakeHouseMenuV2 pancakeHouseMenu, DinerMenuV2 dinerMenu) {
        this.pancakeHouseMenu = pancakeHouseMenu;
        this.dinerMenu = dinerMenu;
    }
    //报全部菜单
    public void printMenu(){
        System.out.println("早餐菜单:");
        MenuIteratorV2 pancakeHouseMenuIterator = pancakeHouseMenu.createIterator();
        printMenu(pancakeHouseMenuIterator);
        System.out.println("午餐菜单:");
        MenuIteratorV2 dinerMenuIterator = dinerMenu.createIterator();
        printMenu(dinerMenuIterator);
    }

    private void printMenu(MenuIteratorV2 menuIterator) {
        while(menuIterator.hasNext()){
            MenuItem menuItem = menuIterator.next();
            System.out.println(menuItem.getName() + "\t" + menuItem.getPrice() + "\t" + menuItem.getDesc());
        }
    }

    //只报早餐菜单
    public void printBreakfastMenu(){

    }

    //只报午餐菜单
    public void printLunchMenu(){}

    //只报素食菜单
    public void printVegetarianMenu(){}

    //指定的菜单项的名称，判断是否为素食
    public void isVegetarian(String name){}
}
