package com.demo.zyy.pattern.itercompose.difinition.compose;

/**
 * 叶子节点
 */
public class Leaf implements Component {

    private String name;

    public Leaf(String name) {
        this.name = name;
    }

    @Override
    public void operation() {
        System.out.println("在"+ name +"叶子上的操作...");
    }

    @Override
    public void add(Component component) {
        //do nothing
    }

    @Override
    public void remove(Component component) {
        //do nothing
    }

}
