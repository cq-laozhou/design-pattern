package com.demo.zyy.pattern.itercompose.menu.v5;

/**
 * 服务员
 */
public class WaitressV5 {

    private MenuComponentV5 rootMenu;

    public WaitressV5(MenuComponentV5 rootMenu) {
        this.rootMenu = rootMenu;
    }

    //报全部菜单
    public void printMenu(){
        System.out.println("菜单:");
        rootMenu.print();
    }

}
