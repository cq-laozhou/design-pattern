package com.demo.zyy.pattern.factory.pizza.v5;

import com.demo.zyy.pattern.factory.pizza.v5.material.Cheese;
import com.demo.zyy.pattern.factory.pizza.v5.material.Clam;
import com.demo.zyy.pattern.factory.pizza.v5.material.Dough;

/**
 * 披萨父类
 */
public abstract class PizzaV5 {

    protected String name;

    protected Dough dough;

    protected Cheese cheese;

    protected Clam  clam;

    //具体的子类披萨知道如何准备自己
    public abstract void prepare();

    public void bake(){
        System.out.println(getName() + " 烘烤...");
    }

    public void cut(){
        System.out.println(getName() + " 切片...");
    }

    public void box(){
        System.out.println(getName() + " 装盒...");
    }

    /**
     * 显示披萨原料
     */
    public void showMaterial(){
        System.out.println("显示披萨原料:");
        dough.display();
        clam.display();
        cheese.display();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
