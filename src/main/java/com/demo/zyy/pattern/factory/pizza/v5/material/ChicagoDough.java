package com.demo.zyy.pattern.factory.pizza.v5.material;

/**
 * 芝加哥版本的面团
 */
public class ChicagoDough implements Dough {
    @Override
    public void display() {
        System.out.println("芝加哥版本的面团");
    }
}
