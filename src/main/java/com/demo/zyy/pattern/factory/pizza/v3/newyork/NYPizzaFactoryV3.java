package com.demo.zyy.pattern.factory.pizza.v3.newyork;

import com.demo.zyy.pattern.factory.pizza.common.Pizza;
import com.demo.zyy.pattern.factory.pizza.v3.PizzaFactoryV3;
import com.demo.zyy.pattern.factory.pizza.v3.chicago.ChicagoCheesePizza;
import com.demo.zyy.pattern.factory.pizza.v3.chicago.ChicagoClamPizza;

/**
 * 纽约的披萨工厂（简单工厂）
 */
public class NYPizzaFactoryV3 implements PizzaFactoryV3 {

    @Override
    public Pizza createPizzaByType(String type){
        Pizza pizza;

        if("cheese".equals(type)){
            pizza = new NYCheesePizza();
        }else if("clam".equals(type)){
            pizza = new NYClamPizza();
        }else{
            pizza = new NYCheesePizza();
        }

        return pizza;
    }
}
