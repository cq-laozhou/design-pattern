package com.demo.zyy.pattern.factory.pizza.v5.material;

/**
 * 纽约版本的蛤蜊（新鲜的蛤蜊）
 */
public class NYClam implements Clam {
    @Override
    public void display() {
        System.out.println("纽约版本的蛤蜊");
    }
}
