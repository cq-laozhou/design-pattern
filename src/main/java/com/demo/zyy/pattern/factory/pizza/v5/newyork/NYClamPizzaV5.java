package com.demo.zyy.pattern.factory.pizza.v5.newyork;

import com.demo.zyy.pattern.factory.pizza.v5.PizzaV5;
import com.demo.zyy.pattern.factory.pizza.v5.material.MaterialFactory;
import com.demo.zyy.pattern.factory.pizza.v5.material.NYDough;

/**
 * 纽约味蛤蜊披萨
 */
public class NYClamPizzaV5 extends PizzaV5{

    MaterialFactory materialFactory;

    public NYClamPizzaV5(MaterialFactory materialFactory) {
        setName("纽约味蛤蜊披萨");
        this.materialFactory = materialFactory;
    }

    @Override
    public void prepare() {
        System.out.println("纽约味蛤蜊披萨，必须有蛤蜊啊");
        dough = materialFactory.createDough();
        cheese = materialFactory.createCheese();
        clam = materialFactory.createClam();
    }
}
