package com.demo.zyy.pattern.factory.pizza.v4.newyork;

import com.demo.zyy.pattern.factory.pizza.common.Pizza;

/**
 * 纽约味芝士披萨
 */
public class NYCheesePizzaV4 extends Pizza{

    public NYCheesePizzaV4() {
        setName("纽约味芝士披萨");
    }

    @Override
    public void prepare() {
        System.out.println("芝士披萨需要准备些芝士、芝麻、番茄、火腿，当然还有面团");
    }
}
