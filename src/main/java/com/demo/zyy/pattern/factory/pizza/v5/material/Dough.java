package com.demo.zyy.pattern.factory.pizza.v5.material;

/**
 * 面团
 */
public interface Dough {
    void display();
}
