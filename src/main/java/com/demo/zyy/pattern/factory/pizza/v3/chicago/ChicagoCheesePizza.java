package com.demo.zyy.pattern.factory.pizza.v3.chicago;

import com.demo.zyy.pattern.factory.pizza.common.Pizza;

/**
 * 芝加哥味芝士披萨
 */
public class ChicagoCheesePizza extends Pizza{

    public ChicagoCheesePizza() {
        setName("芝加哥味芝士披萨");
    }

    @Override
    public void prepare() {
        System.out.println("芝士披萨需要准备些芝士、芝麻、番茄、火腿，当然还有面团");
    }
}
