package com.demo.zyy.pattern.factory.definition.factorymethod;

/**
 * 要被创建的产品抽象
 */
public interface Product {
    void showMe();
}
