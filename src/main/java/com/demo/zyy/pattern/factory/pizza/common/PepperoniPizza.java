package com.demo.zyy.pattern.factory.pizza.common;

/**
 * 意式披萨
 */
public class PepperoniPizza extends Pizza{
    public PepperoniPizza() {
        setName("意式披萨");
    }
    @Override
    public void prepare() {
        System.out.println("意式披萨需要准备些芝麻、番茄、意大利火腿，橄榄油等，当然还有面团");
    }
}
