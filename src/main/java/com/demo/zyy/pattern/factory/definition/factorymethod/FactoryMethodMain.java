package com.demo.zyy.pattern.factory.definition.factorymethod;

/**
 * 工厂方法模式测试类
 */
public class FactoryMethodMain {
    public static void main(String[] args) {
        Creator creatorA = new ConcreteCreatorA();
        creatorA.showProduct();

        Creator creatorB = new ConcreteCreatorB();
        creatorB.showProduct();
    }
}
