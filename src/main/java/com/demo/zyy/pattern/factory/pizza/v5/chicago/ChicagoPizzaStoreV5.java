package com.demo.zyy.pattern.factory.pizza.v5.chicago;

import com.demo.zyy.pattern.factory.pizza.v5.PizzaStoreV5;
import com.demo.zyy.pattern.factory.pizza.v5.PizzaV5;
import com.demo.zyy.pattern.factory.pizza.v5.material.MaterialFactory;

/**
 * 芝加哥分店
 */
public class ChicagoPizzaStoreV5 extends PizzaStoreV5 {
    @Override
    protected PizzaV5 createPizzaByType(String type) {
        PizzaV5 pizza;

        MaterialFactory materialFactory = new ChicagoMaterialFactory();

        if("cheese".equals(type)){
            pizza = new ChicagoCheesePizzaV5(materialFactory);
        }else if("clam".equals(type)){
            pizza = new ChicagoClamPizzaV5(materialFactory);
        }else{
            pizza = new ChicagoCheesePizzaV5(materialFactory);
        }
        return pizza;
    }
}
