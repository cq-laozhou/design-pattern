package com.demo.zyy.pattern.factory.definition.abstractfactory;

/**
 * 产品B的具体实现B2
 */
public class ConcreteProductB2 implements ProductMemberB {
    @Override
    public void showB() {
        System.out.println("我是B2");
    }
}
