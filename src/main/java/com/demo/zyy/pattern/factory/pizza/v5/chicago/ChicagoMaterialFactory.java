package com.demo.zyy.pattern.factory.pizza.v5.chicago;

import com.demo.zyy.pattern.factory.pizza.v5.material.Cheese;
import com.demo.zyy.pattern.factory.pizza.v5.material.ChicagoCheese;
import com.demo.zyy.pattern.factory.pizza.v5.material.ChicagoClam;
import com.demo.zyy.pattern.factory.pizza.v5.material.ChicagoDough;
import com.demo.zyy.pattern.factory.pizza.v5.material.Clam;
import com.demo.zyy.pattern.factory.pizza.v5.material.Dough;
import com.demo.zyy.pattern.factory.pizza.v5.material.MaterialFactory;
import com.demo.zyy.pattern.factory.pizza.v5.material.NYCheese;
import com.demo.zyy.pattern.factory.pizza.v5.material.NYClam;
import com.demo.zyy.pattern.factory.pizza.v5.material.NYDough;

/**
 * 芝加哥的原料工厂
 */
public class ChicagoMaterialFactory implements MaterialFactory {
    @Override
    public Dough createDough() {
        return new ChicagoDough();
    }

    @Override
    public Cheese createCheese() {
        return new ChicagoCheese();
    }

    @Override
    public Clam createClam() {
        return new ChicagoClam();
    }
}
