package com.demo.zyy.pattern.factory.pizza.v4.chicago;

import com.demo.zyy.pattern.factory.pizza.common.Pizza;
import com.demo.zyy.pattern.factory.pizza.v4.PizzaStoreV4;

/**
 * 芝加哥分店
 */
public class ChicagoPizzaStoreV4 extends PizzaStoreV4 {
    @Override
    protected Pizza createPizzaByType(String type) {
        Pizza pizza;

        if("cheese".equals(type)){
            pizza = new ChicagoCheesePizzaV4();
        }else if("clam".equals(type)){
            pizza = new ChicagoClamPizzaV4();
        }else{
            pizza = new ChicagoCheesePizzaV4();
        }
        return pizza;
    }
}
