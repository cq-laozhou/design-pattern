package com.demo.zyy.pattern.factory.definition.abstractfactory;

/**
 * 测试
 */
public class AbstractFactoryMain {

    public static void main(String[] args) {
        //系列1产品
        ProductMemberFactory factory = new ConcreteProductSeries1Factory();
        factory.createProductA().showA();
        factory.createProductB().showB();

        System.out.println();
        //系列2产品
        factory = new ConcreteProductSeries2Factory();
        factory.createProductA().showA();
        factory.createProductB().showB();
    }
}
