package com.demo.zyy.pattern.factory.definition.factorymethod;

/**
 * 具体创建者A
 */
public class ConcreteCreatorA extends Creator{
    @Override
    protected Product createProduct() {
        return new ConcreteProductA();
    }
}
