package com.demo.zyy.pattern.factory.pizza.v5;

/**
 * V5版披萨店
 */
public abstract class PizzaStoreV5 {
    
    /**
     * 下单
     * @param type 披萨类型
     * @return
     */
    public PizzaV5 orderPizza(String type){
        PizzaV5 pizza = createPizzaByType(type);

        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();

        pizza.showMaterial();

        return pizza;

    }

    protected abstract PizzaV5 createPizzaByType(String type);
}
