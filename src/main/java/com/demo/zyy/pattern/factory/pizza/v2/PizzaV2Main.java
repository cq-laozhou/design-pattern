package com.demo.zyy.pattern.factory.pizza.v2;

import com.demo.zyy.pattern.factory.pizza.v1.PizzaStoreV1;

/**
 * V2版测试
 */
public class PizzaV2Main {
    public static void main(String[] args) {
        PizzaStoreV2 pizzaStore = new PizzaStoreV2(new PizzaFactoryV2());
        //下单一个
        pizzaStore.orderPizza("clam");

        pizzaStore.orderPizza("veggie");
    }
}
