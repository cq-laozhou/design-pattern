package com.demo.zyy.pattern.factory.definition.abstractfactory;

/**
 * 产品A的具体实现A1
 */
public class ConcreteProductA1 implements ProductMemberA {
    @Override
    public void showA() {
        System.out.println("我是A1");
    }
}
