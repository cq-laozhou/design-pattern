package com.demo.zyy.pattern.factory.pizza.v4.chicago;

import com.demo.zyy.pattern.factory.pizza.common.Pizza;

/**
 * 芝加哥味蛤蜊披萨
 */
public class ChicagoClamPizzaV4 extends Pizza{
    public ChicagoClamPizzaV4() {
        setName("芝加哥味蛤蜊披萨");
    }

    @Override
    public void prepare() {
        System.out.println("蛤蜊披萨，必须有蛤蜊啊");
    }
}
