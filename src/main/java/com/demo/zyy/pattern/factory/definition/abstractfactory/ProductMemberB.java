package com.demo.zyy.pattern.factory.definition.abstractfactory;

/**
 * 产品家族成员B
 */
public interface ProductMemberB {
    void showB();
}
