package com.demo.zyy.pattern.factory.definition.abstractfactory;

/**
 * 具体工厂（系列1）
 */
public class ConcreteProductSeries2Factory implements ProductMemberFactory{
    @Override
    public ProductMemberA createProductA() {
        return new ConcreteProductA2();
    }

    @Override
    public ProductMemberB createProductB() {
        return new ConcreteProductB2();
    }
}
