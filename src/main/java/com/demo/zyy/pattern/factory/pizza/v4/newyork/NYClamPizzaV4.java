package com.demo.zyy.pattern.factory.pizza.v4.newyork;

import com.demo.zyy.pattern.factory.pizza.common.Pizza;

/**
 * 纽约味蛤蜊披萨
 */
public class NYClamPizzaV4 extends Pizza{
    public NYClamPizzaV4() {
        setName("纽约味蛤蜊披萨");
    }

    @Override
    public void prepare() {
        System.out.println("蛤蜊披萨，必须有蛤蜊啊");
    }
}
