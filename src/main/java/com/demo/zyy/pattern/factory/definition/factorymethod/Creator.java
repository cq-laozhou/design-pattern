package com.demo.zyy.pattern.factory.definition.factorymethod;

/**
 * 抽象的创建者
 */
public abstract class Creator {

    /**
     * 这个方法就是工厂方法
     * @return
     */
    protected  abstract Product createProduct();

    /**
     * 其他方法可以使用工厂方法获取产品对象，而不用关心产品对象是怎么创建出来的。
     */
    public void showProduct(){
        Product product = createProduct();
        product.showMe();
    }
}
