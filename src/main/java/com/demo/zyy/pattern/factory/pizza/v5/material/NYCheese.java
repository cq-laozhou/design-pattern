package com.demo.zyy.pattern.factory.pizza.v5.material;

/**
 * 纽约版本奶酪
 */
public class NYCheese implements Cheese {
    @Override
    public void display() {
        System.out.println("纽约版本奶酪");
    }
}
