package com.demo.zyy.pattern.factory.pizza.v5.newyork;

import com.demo.zyy.pattern.factory.pizza.v5.PizzaStoreV5;
import com.demo.zyy.pattern.factory.pizza.v5.PizzaV5;
import com.demo.zyy.pattern.factory.pizza.v5.material.MaterialFactory;

/**
 * 纽约分店
 */
public class NYPizzaStoreV5 extends PizzaStoreV5 {
    @Override
    protected PizzaV5 createPizzaByType(String type) {
        PizzaV5 pizza;
        MaterialFactory materialFactory = new NYMaterialFactory();

        if("cheese".equals(type)){
            pizza = new NYCheesePizzaV5(materialFactory);
        }else if("clam".equals(type)){
            pizza = new NYClamPizzaV5(materialFactory);
        }else{
            pizza = new NYCheesePizzaV5(materialFactory);
        }
        return pizza;
    }
}
