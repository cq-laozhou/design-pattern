package com.demo.zyy.pattern.factory.pizza.v2;

import com.demo.zyy.pattern.factory.pizza.common.Pizza;

/**
 * V2版披萨店
 */
public class PizzaStoreV2 {

    PizzaFactoryV2 pizzaFactory;

    public PizzaStoreV2(PizzaFactoryV2 pizzaFactory) {
        this.pizzaFactory = pizzaFactory;
    }

    /**
     * 下单
     * @param type 披萨类型
     * @return
     */
    public Pizza orderPizza(String type){
        Pizza pizza = pizzaFactory.createPizzaByType(type);

        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();

        return pizza;

    }
}
