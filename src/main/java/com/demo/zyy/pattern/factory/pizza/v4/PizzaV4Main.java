package com.demo.zyy.pattern.factory.pizza.v4;

import com.demo.zyy.pattern.factory.pizza.v4.chicago.ChicagoPizzaStoreV4;
import com.demo.zyy.pattern.factory.pizza.v4.newyork.NYPizzaStoreV4;

/**
 * V4版测试
 */
public class PizzaV4Main {
    public static void main(String[] args) {
        //纽约的店
        PizzaStoreV4 nyPizzaStore = new NYPizzaStoreV4();
        //下单
        nyPizzaStore.orderPizza("clam");
        nyPizzaStore.orderPizza("cheese");

        //芝加哥的店
        PizzaStoreV4 chicagoPizzaStore = new ChicagoPizzaStoreV4();
        chicagoPizzaStore.orderPizza("clam");
        chicagoPizzaStore.orderPizza("cheese");
    }
}
