package com.demo.zyy.pattern.factory.pizza.v5.newyork;

import com.demo.zyy.pattern.factory.pizza.v5.material.Cheese;
import com.demo.zyy.pattern.factory.pizza.v5.material.Clam;
import com.demo.zyy.pattern.factory.pizza.v5.material.Dough;
import com.demo.zyy.pattern.factory.pizza.v5.material.MaterialFactory;
import com.demo.zyy.pattern.factory.pizza.v5.material.NYCheese;
import com.demo.zyy.pattern.factory.pizza.v5.material.NYClam;
import com.demo.zyy.pattern.factory.pizza.v5.material.NYDough;

/**
 * 纽约的原料工厂
 */
public class NYMaterialFactory implements MaterialFactory {
    @Override
    public Dough createDough() {
        return new NYDough();
    }

    @Override
    public Cheese createCheese() {
        return new NYCheese();
    }

    @Override
    public Clam createClam() {
        return new NYClam();
    }
}
