package com.demo.zyy.pattern.factory.pizza.common;

/**
 * 蔬菜披萨
 */
public class VeggiePizza extends Pizza{
    public VeggiePizza() {
        setName("蔬菜披萨");
    }
    @Override
    public void prepare() {
        System.out.println("蔬菜披萨全TM是素的，吃毛线...");
    }
}
