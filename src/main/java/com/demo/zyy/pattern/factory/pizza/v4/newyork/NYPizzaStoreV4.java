package com.demo.zyy.pattern.factory.pizza.v4.newyork;

import com.demo.zyy.pattern.factory.pizza.common.Pizza;
import com.demo.zyy.pattern.factory.pizza.v4.PizzaStoreV4;

/**
 * 纽约分店
 */
public class NYPizzaStoreV4 extends PizzaStoreV4 {
    @Override
    protected Pizza createPizzaByType(String type) {
        Pizza pizza;

        if("cheese".equals(type)){
            pizza = new NYCheesePizzaV4();
        }else if("clam".equals(type)){
            pizza = new NYClamPizzaV4();
        }else{
            pizza = new NYCheesePizzaV4();
        }
        return pizza;
    }
}
