package com.demo.zyy.pattern.factory.pizza.v1;

/**
 * V1版测试
 */
public class PizzaV1Main {
    public static void main(String[] args) {
        PizzaStoreV1 pizzaStore = new PizzaStoreV1();
        //下单一个
        pizzaStore.orderPizza("clam");

        pizzaStore.orderPizza("veggie");
    }
}
