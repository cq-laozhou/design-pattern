package com.demo.zyy.pattern.factory.pizza.v3.chicago;

import com.demo.zyy.pattern.factory.pizza.common.Pizza;
import com.demo.zyy.pattern.factory.pizza.v3.PizzaFactoryV3;

/**
 * 芝加哥的披萨工厂（简单工厂）
 */
public class ChicagoPizzaFactoryV3 implements PizzaFactoryV3{

    @Override
    public Pizza createPizzaByType(String type){
        Pizza pizza;

        if("cheese".equals(type)){
            pizza = new ChicagoCheesePizza();
        }else if("clam".equals(type)){
            pizza = new ChicagoClamPizza();
        }else{
            pizza = new ChicagoCheesePizza();//不知道类型时，默认芝士披萨
        }

        return pizza;
    }
}
