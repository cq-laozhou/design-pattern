package com.demo.zyy.pattern.factory.pizza.v1;

import com.demo.zyy.pattern.factory.pizza.common.CheesePizza;
import com.demo.zyy.pattern.factory.pizza.common.ClamPizza;
import com.demo.zyy.pattern.factory.pizza.common.PepperoniPizza;
import com.demo.zyy.pattern.factory.pizza.common.Pizza;
import com.demo.zyy.pattern.factory.pizza.common.VeggiePizza;

/**
 * V1版披萨店
 */
public class PizzaStoreV1 {

    /**
     * 下单
     * @param type 披萨类型
     * @return
     */
    public Pizza orderPizza(String type){
        Pizza pizza;

        if("cheese".equals(type)){
            pizza = new CheesePizza();
        }else if("pepperoni".equals(type)){
            pizza = new PepperoniPizza();
        }else if("clam".equals(type)){
            pizza = new ClamPizza();
        }else if("veggie".equals(type)){
            pizza = new VeggiePizza();
        }else{
            pizza = new CheesePizza();//不知道类型时，默认芝士披萨
        }

        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();

        return pizza;

    }
}
