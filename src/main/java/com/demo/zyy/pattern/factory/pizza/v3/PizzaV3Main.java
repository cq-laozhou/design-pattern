package com.demo.zyy.pattern.factory.pizza.v3;

import com.demo.zyy.pattern.factory.pizza.v3.chicago.ChicagoPizzaFactoryV3;
import com.demo.zyy.pattern.factory.pizza.v3.newyork.NYPizzaFactoryV3;

/**
 * V3版测试
 */
public class PizzaV3Main {
    public static void main(String[] args) {
        //纽约的店
        PizzaStoreV3 nyPizzaStore = new PizzaStoreV3(new NYPizzaFactoryV3());
        //下单
        nyPizzaStore.orderPizza("clam");
        nyPizzaStore.orderPizza("cheese");

        //芝加哥的店
        PizzaStoreV3 chicagoPizzaStore = new PizzaStoreV3(new ChicagoPizzaFactoryV3());
        chicagoPizzaStore.orderPizza("clam");
        chicagoPizzaStore.orderPizza("cheese");
    }
}
