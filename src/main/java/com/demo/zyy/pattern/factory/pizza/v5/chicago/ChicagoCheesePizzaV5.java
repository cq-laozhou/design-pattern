package com.demo.zyy.pattern.factory.pizza.v5.chicago;

import com.demo.zyy.pattern.factory.pizza.v5.PizzaV5;
import com.demo.zyy.pattern.factory.pizza.v5.material.MaterialFactory;

/**
 * 芝加哥味芝士披萨
 */
public class ChicagoCheesePizzaV5 extends PizzaV5{

    MaterialFactory materialFactory;

    public ChicagoCheesePizzaV5(MaterialFactory materialFactory) {
        setName("芝加哥味芝士披萨");
        this.materialFactory = materialFactory;
    }

    @Override
    public void prepare() {
        System.out.println("芝士披萨需要准备些芝士、芝麻、番茄、火腿，当然还有面团");
        dough = materialFactory.createDough();
        cheese = materialFactory.createCheese();
        clam = materialFactory.createClam();
    }
}
