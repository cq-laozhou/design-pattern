package com.demo.zyy.pattern.factory.definition.abstractfactory;

/**
 * 产品A的具体实现A2
 */
public class ConcreteProductA2 implements ProductMemberA {
    @Override
    public void showA() {
        System.out.println("我是A2");
    }
}
