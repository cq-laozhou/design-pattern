package com.demo.zyy.pattern.factory.definition.factorymethod;

/**
 * 具体创建者B
 */
public class ConcreteCreatorB extends Creator{
    @Override
    protected Product createProduct() {
        return new ConcreteProductB();
    }
}
