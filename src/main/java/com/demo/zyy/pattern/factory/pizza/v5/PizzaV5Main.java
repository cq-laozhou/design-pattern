package com.demo.zyy.pattern.factory.pizza.v5;

import com.demo.zyy.pattern.factory.pizza.v5.chicago.ChicagoPizzaStoreV5;
import com.demo.zyy.pattern.factory.pizza.v5.newyork.NYPizzaStoreV5;

/**
 * V5版测试
 */
public class PizzaV5Main {
    public static void main(String[] args) {
        //纽约的店
        PizzaStoreV5 nyPizzaStore = new NYPizzaStoreV5();
        //下单
        nyPizzaStore.orderPizza("clam");
        nyPizzaStore.orderPizza("cheese");

        //芝加哥的店
        PizzaStoreV5 chicagoPizzaStore = new ChicagoPizzaStoreV5();
        chicagoPizzaStore.orderPizza("clam");
        chicagoPizzaStore.orderPizza("cheese");
    }
}
