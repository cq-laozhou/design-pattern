package com.demo.zyy.pattern.factory.definition.factorymethod;

/**
 * 具体的产品A
 */
public class ConcreteProductA implements Product {
    @Override
    public void showMe() {
        System.out.println("具体产品A");
    }
}
