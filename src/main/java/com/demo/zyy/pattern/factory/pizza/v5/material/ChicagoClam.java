package com.demo.zyy.pattern.factory.pizza.v5.material;

/**
 * 芝加哥版本的蛤蜊
 */
public class ChicagoClam implements Clam {
    @Override
    public void display() {
        System.out.println("芝加哥版本的蛤蜊");
    }
}
