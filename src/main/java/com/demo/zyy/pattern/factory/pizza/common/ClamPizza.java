package com.demo.zyy.pattern.factory.pizza.common;

/**
 * 蛤蜊披萨
 */
public class ClamPizza extends Pizza{
    public ClamPizza() {
        setName("蛤蜊披萨");
    }

    @Override
    public void prepare() {
        System.out.println("蛤蜊披萨，必须有蛤蜊啊");
    }
}
