package com.demo.zyy.pattern.factory.definition.abstractfactory;

/**
 * 产品B的具体实现B1
 */
public class ConcreteProductB1 implements ProductMemberB {
    @Override
    public void showB() {
        System.out.println("我是B1");
    }
}
