package com.demo.zyy.pattern.factory.pizza.v3;

import com.demo.zyy.pattern.factory.pizza.common.Pizza;
import com.demo.zyy.pattern.factory.pizza.v2.PizzaFactoryV2;

/**
 * V3版披萨店
 */
public class PizzaStoreV3 {

    PizzaFactoryV3 pizzaFactory;

    public PizzaStoreV3(PizzaFactoryV3 pizzaFactory) {
        this.pizzaFactory = pizzaFactory;
    }

    /**
     * 下单
     * @param type 披萨类型
     * @return
     */
    public Pizza orderPizza(String type){
        Pizza pizza = pizzaFactory.createPizzaByType(type);

        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();

        return pizza;

    }
}
