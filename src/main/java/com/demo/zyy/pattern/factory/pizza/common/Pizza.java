package com.demo.zyy.pattern.factory.pizza.common;

/**
 * 披萨父类
 */
public abstract class Pizza {

    protected String name;

    //具体的子类披萨知道如何准备自己
    public abstract void prepare();

    public void bake(){
        System.out.println(getName() + " 烘烤...");
    }

    public void cut(){
        System.out.println(getName() + " 切片...");
    }

    public void box(){
        System.out.println(getName() + " 装盒...");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
