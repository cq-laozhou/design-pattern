package com.demo.zyy.pattern.factory.pizza.v5.material;

/**
 * 奶酪
 */
public interface Cheese {
    void display();
}
