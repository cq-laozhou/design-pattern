package com.demo.zyy.pattern.factory.pizza.v4;

import com.demo.zyy.pattern.factory.pizza.common.Pizza;
import com.demo.zyy.pattern.factory.pizza.v3.PizzaFactoryV3;

/**
 * V4版披萨店
 */
public abstract class PizzaStoreV4 {
    
    /**
     * 下单
     * @param type 披萨类型
     * @return
     */
    public Pizza orderPizza(String type){
        Pizza pizza = createPizzaByType(type);

        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();

        return pizza;

    }

    protected abstract Pizza createPizzaByType(String type);
}
