package com.demo.zyy.pattern.factory.pizza.v5.material;

/**
 * 芝加哥版本奶酪
 */
public class ChicagoCheese implements Cheese {
    @Override
    public void display() {
        System.out.println("芝加哥版本奶酪");
    }
}
