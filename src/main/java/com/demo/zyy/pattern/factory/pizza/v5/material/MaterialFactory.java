package com.demo.zyy.pattern.factory.pizza.v5.material;

/**
 * 原料工厂接口
 */
public interface MaterialFactory {

    Dough createDough();
    Cheese createCheese();
    Clam  createClam();

}
