package com.demo.zyy.pattern.factory.definition.abstractfactory;

/**
 * 产品家族工厂
 */
public interface ProductMemberFactory {
    ProductMemberA createProductA();
    ProductMemberB createProductB();
}
