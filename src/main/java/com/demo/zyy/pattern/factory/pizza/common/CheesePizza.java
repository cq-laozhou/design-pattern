package com.demo.zyy.pattern.factory.pizza.common;

/**
 * 芝士披萨
 */
public class CheesePizza extends Pizza{

    public CheesePizza() {
        setName("芝士披萨");
    }

    @Override
    public void prepare() {
        System.out.println("芝士披萨需要准备些芝士、芝麻、番茄、火腿，当然还有面团");
    }
}
