package com.demo.zyy.pattern.factory.definition.factorymethod;

/**
 * 具体的产品B
 */
public class ConcreteProductB implements Product {
    @Override
    public void showMe() {
        System.out.println("具体产品B");
    }
}
