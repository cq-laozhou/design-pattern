package com.demo.zyy.pattern.factory.pizza.v5.newyork;

import com.demo.zyy.pattern.factory.pizza.v5.PizzaV5;
import com.demo.zyy.pattern.factory.pizza.v5.material.MaterialFactory;

/**
 * 纽约味芝士披萨
 */
public class NYCheesePizzaV5 extends PizzaV5{

    MaterialFactory materialFactory;

    public NYCheesePizzaV5(MaterialFactory materialFactory) {
        setName("纽约味芝士披萨");
        this.materialFactory = materialFactory;
    }

    @Override
    public void prepare() {
        System.out.println("芝士披萨需要准备些芝士、芝麻、番茄、火腿，当然还有面团");
        dough = materialFactory.createDough();
        cheese = materialFactory.createCheese();
        clam = materialFactory.createClam();
    }
}
