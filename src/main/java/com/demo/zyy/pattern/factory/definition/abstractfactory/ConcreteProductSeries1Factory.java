package com.demo.zyy.pattern.factory.definition.abstractfactory;

/**
 * 具体工厂（系列1）
 */
public class ConcreteProductSeries1Factory implements ProductMemberFactory{
    @Override
    public ProductMemberA createProductA() {
        return new ConcreteProductA1();
    }

    @Override
    public ProductMemberB createProductB() {
        return new ConcreteProductB1();
    }
}
