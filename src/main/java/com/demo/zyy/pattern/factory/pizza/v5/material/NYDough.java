package com.demo.zyy.pattern.factory.pizza.v5.material;

/**
 * 纽约版本的面团
 */
public class NYDough implements Dough {
    @Override
    public void display() {
        System.out.println("纽约版本的面团");
    }
}
