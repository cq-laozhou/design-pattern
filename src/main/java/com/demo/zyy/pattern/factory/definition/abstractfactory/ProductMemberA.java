package com.demo.zyy.pattern.factory.definition.abstractfactory;

/**
 * 产品家族成员A
 */
public interface ProductMemberA {
    void showA();
}
