package com.demo.zyy.pattern.factory.pizza.v3;

import com.demo.zyy.pattern.factory.pizza.common.Pizza;

/**
 * 披萨工厂抽象
 */
public interface PizzaFactoryV3 {
    Pizza createPizzaByType(String type);
}
