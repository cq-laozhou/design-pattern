package com.demo.zyy.pattern.decorator.cafe.v2;

/**
 * 糖
 */
public class SugarV2 implements FlavouringV2{
    @Override
    public String getName() {
        return "甜甜的糖";
    }
    @Override
    public float cost() {
        return 2.15f;
    }
}
