package com.demo.zyy.pattern.decorator.cafe.v2;

/**
 * 奶
 */
public class MilkV2 implements FlavouringV2{
    @Override
    public String getName() {
        return "白白的牛奶";
    }
    @Override
    public float cost() {
        return 1.2f;
    }
}
