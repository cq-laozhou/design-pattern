package com.demo.zyy.pattern.decorator.room;

/**
 * 颜色装饰者
 */
public class ColorDecorator extends RoomDecorator{
    public ColorDecorator(Room specialRoom) {
        super(specialRoom);
    }

    @Override
    public String showRoom() {
        return specialRoom.showRoom() + addColors();
    }

    private String addColors() {
        return " 被涂成蓝色 ";
    }
}
