package com.demo.zyy.pattern.decorator.cafe.v3;

/**
 * 奶装饰者
 */
public class MilkDecoratorV3 extends FlavouringDecoratorV3{

    public MilkDecoratorV3(BeverageV3 beverage) {
        super(beverage);
        setDescription("白白的奶");
    }

    @Override
    public float cost() {
        return 1.2f + beverage.cost();
    }
}
