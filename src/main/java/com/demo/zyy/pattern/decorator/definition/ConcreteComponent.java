package com.demo.zyy.pattern.decorator.definition;

/**
 * 具体组件
 */
public class ConcreteComponent implements Component {
    @Override
    public void methodA() {
        System.out.println("具体组件实现方法A");
    }
}
