package com.demo.zyy.pattern.decorator.cafe.v2;

/**
 * 深度烘焙咖啡
 */
public class DarkRoastV2 extends BeverageV2 {
    public DarkRoastV2() {
        setDescription("深度烘焙咖啡");
    }
    @Override
    public float cost() {
        return 9.05f + costFlavouring();
    }
}
