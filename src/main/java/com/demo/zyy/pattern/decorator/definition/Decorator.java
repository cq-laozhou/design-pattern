package com.demo.zyy.pattern.decorator.definition;

/**
 * 抽象装饰者
 */
public abstract class Decorator implements Component{
    Component component; //被装饰对象

    public Decorator(Component component) {
        this.component = component;
    }
}
