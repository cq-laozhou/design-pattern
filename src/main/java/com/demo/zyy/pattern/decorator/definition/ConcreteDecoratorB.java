package com.demo.zyy.pattern.decorator.definition;

/**
 * 具体装饰者A
 */
public class ConcreteDecoratorB extends Decorator {
    public ConcreteDecoratorB(Component component) {
        super(component);
    }

    @Override
    public void methodA() {
        System.out.println("具体装饰者B开始装饰.." );
        component.methodA();
        System.out.println("具体装饰者B装饰结束.." );
    }
}
