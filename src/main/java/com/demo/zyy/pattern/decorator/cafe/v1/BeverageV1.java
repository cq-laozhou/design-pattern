package com.demo.zyy.pattern.decorator.cafe.v1;

/**
 * 饮料基类
 */
public abstract class BeverageV1 {
    private String description;

    /**
     * 计算价格方法
     * @return
     */
    public abstract float cost();

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
