package com.demo.zyy.pattern.decorator.definition;

/**
 * 具体装饰者A
 */
public class ConcreteDecoratorA extends Decorator {
    public ConcreteDecoratorA(Component component) {
        super(component);
    }

    @Override
    public void methodA() {
        System.out.println("具体装饰者A开始装饰.." );
        component.methodA();
        System.out.println("具体装饰者A装饰结束.." );
    }
}
