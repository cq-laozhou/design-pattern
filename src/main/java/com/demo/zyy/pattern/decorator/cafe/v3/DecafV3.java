package com.demo.zyy.pattern.decorator.cafe.v3;

/**
 * 脱因咖啡
 */
public class DecafV3 extends BeverageV3 {
    public DecafV3() {
        setDescription("脱因咖啡");
    }

    @Override
    public float cost() {
        return .82f;
    }
}
