package com.demo.zyy.pattern.decorator.definition;

/**
 * 测试
 */
public class DecoratorMain {
    public static void main(String[] args) {
        Component component =
                new ConcreteDecoratorB(new ConcreteDecoratorA(new ConcreteComponent()));
        component.methodA();
    }
}
