package com.demo.zyy.pattern.decorator.cafe.v1;

/**
 * 深度烘焙咖啡
 */
public class DarkRoastV1 extends BeverageV1 {
    @Override
    public float cost() {
        return 1.05f;
    }
}
