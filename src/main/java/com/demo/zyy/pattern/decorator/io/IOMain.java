package com.demo.zyy.pattern.decorator.io;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringBufferInputStream;

/**
 * IO测试
 */
public class IOMain {
    public static void main(String[] args) throws IOException {
        InputStream is =
                new LowerCaseInputStream(new BufferedInputStream(new StringBufferInputStream("THIS is The TEST text")));
        int c ;
        while((c = is.read()) > 0){
            System.out.print((char)c);
        }
    }
}
