package com.demo.zyy.pattern.decorator.cafe.v2;

import java.util.ArrayList;
import java.util.List;

/**
 * 饮料基类
 */
public abstract class BeverageV2 {
    private String description;

    private List<FlavouringV2> flavourings; //调料列表

    public BeverageV2() {
        flavourings = new ArrayList<>();
    }

    /**
     * 添加调料
     * @param flavouring
     */
    public void addFlavouring(FlavouringV2 flavouring){
        flavourings.add(flavouring);
    }

    /**
     * 计算调料价格
     * @return
     */
    public float costFlavouring(){
        //将所有调料的价格汇总返回
        return flavourings.stream().map(f -> f.cost()).reduce((c1,c2) -> c1+c2).orElse(0.0f);
    }

    /**
     * 计算价格方法
     * @return
     */
    public abstract float cost();

    public String getDescription() {
        String flavouringNames = combineFlavouringNames();
        String concatString = flavouringNames.isEmpty() ? ", " : ",加了 ";
        return description + concatString + flavouringNames;
    }

    private String combineFlavouringNames() {
        return flavourings.stream().map(f -> f.getName()).reduce((name1,name2) -> name1+","+name2).orElse("");
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
