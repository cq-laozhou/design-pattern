package com.demo.zyy.pattern.decorator.room;

/**
 * 房间装饰器（装饰器父类）
 */
public class RoomDecorator implements Room {

    //被装饰的房间
    protected Room specialRoom;

    public RoomDecorator(Room specialRoom) {
        this.specialRoom = specialRoom;
    }

    @Override
    public String showRoom() {
        return specialRoom.showRoom();
    }
}
