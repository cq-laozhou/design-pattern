package com.demo.zyy.pattern.decorator.cafe.v2;

/**
 * V2版测试
 */
public class CafeMainV2 {
    public static void main(String[] args) {
        //点一杯脱因咖啡，什么都不加
        BeverageV2 cafe1 = new DecafV2();
        System.out.println(cafe1.getDescription() + " 需要" +cafe1.cost()+"元");

        //点一杯脱因咖啡，加奶
        BeverageV2 cafe2 = new DecafV2();
        cafe2.addFlavouring(new MilkV2());
        System.out.println(cafe2.getDescription() + " 需要" +cafe2.cost()+"元");

        //点一杯脱因咖啡，加奶,加糖
        BeverageV2 cafe3 = new DecafV2();
        cafe3.addFlavouring(new MilkV2());
        cafe3.addFlavouring(new SugarV2());
        System.out.println(cafe3.getDescription() + " 需要" +cafe3.cost()+"元");
    }
}
