package com.demo.zyy.pattern.decorator.cafe.v1;

/**
 * 脱因咖啡
 */
public class DecafV1 extends BeverageV1 {
    @Override
    public float cost() {
        return .82f;
    }
}
