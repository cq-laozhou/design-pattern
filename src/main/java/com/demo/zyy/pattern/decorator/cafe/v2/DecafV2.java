package com.demo.zyy.pattern.decorator.cafe.v2;

/**
 * 脱因咖啡
 */
public class DecafV2 extends BeverageV2 {
    public DecafV2() {
        setDescription("脱因咖啡");
    }

    @Override
    public float cost() {
        return 6.82f + costFlavouring();
    }
}
