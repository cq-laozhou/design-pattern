package com.demo.zyy.pattern.decorator.room;

/**
 * 窗帘装饰者
 */
public class CurtainDecorator extends RoomDecorator {
    public CurtainDecorator(Room specialRoom) {
        super(specialRoom);
    }

    @Override
    public String showRoom() {
        return specialRoom.showRoom() + addCurtains();
    }

    private String addCurtains() {
        return " 挂上红色的窗帘 ";
    }
}
