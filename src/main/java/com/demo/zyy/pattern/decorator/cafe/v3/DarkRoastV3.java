package com.demo.zyy.pattern.decorator.cafe.v3;

/**
 * 深度烘焙咖啡
 */
public class DarkRoastV3 extends BeverageV3 {
    public DarkRoastV3() {
        setDescription("深度烘焙咖啡");
    }
    @Override
    public float cost() {
        return 1.05f;
    }
}
