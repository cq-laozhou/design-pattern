package com.demo.zyy.pattern.decorator.cafe.v2;

/**
 * 摩卡
 */
public class MochaV2 implements FlavouringV2{
    @Override
    public String getName() {
        return "摩卡-巧克力风味";
    }
    @Override
    public float cost() {
        return 1.15f;
    }
}
