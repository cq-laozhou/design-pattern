package com.demo.zyy.pattern.decorator.cafe.v3;

/**
 * 糖装饰者
 */
public class SugarDecoratorV3 extends FlavouringDecoratorV3{

    public SugarDecoratorV3(BeverageV3 beverage) {
        super(beverage);
        setDescription("甜甜的糖");
    }

    @Override
    public float cost() {
        return 2.15f + beverage.cost();
    }

}
