package com.demo.zyy.pattern.decorator.cafe.v2;

/**
 * 调料基类
 */
public interface FlavouringV2 {

    String getName(); //调料名称

    float cost(); //调料价格
}
