package com.demo.zyy.pattern.decorator.cafe.v3;

import com.demo.zyy.pattern.decorator.cafe.v2.BeverageV2;
import com.demo.zyy.pattern.decorator.cafe.v2.DecafV2;
import com.demo.zyy.pattern.decorator.cafe.v2.MilkV2;
import com.demo.zyy.pattern.decorator.cafe.v2.SugarV2;

/**
 * V3版测试
 */
public class CafeMainV3 {
    public static void main(String[] args) {
        //点一杯脱因咖啡，什么都不加
        BeverageV3 cafe1 = new DecafV3();
        System.out.println(cafe1.getDescription() + " 需要" +cafe1.cost()+"元");

        //点一杯脱因咖啡，加奶
        BeverageV3 cafe2 = new DecafV3();
        FlavouringDecoratorV3 milkDecorator = new MilkDecoratorV3(cafe2);
        System.out.println(milkDecorator.getDescription() + " 需要" +milkDecorator.cost()+"元");

        //点一杯脱因咖啡，加奶,加糖
        BeverageV3 cafe3 = new DecafV3();
        milkDecorator = new MilkDecoratorV3(cafe3);
        FlavouringDecoratorV3 sugarDecorator = new SugarDecoratorV3(milkDecorator);
        System.out.println(sugarDecorator.getDescription() + " 需要" +sugarDecorator.cost()+"元");
    }
}
