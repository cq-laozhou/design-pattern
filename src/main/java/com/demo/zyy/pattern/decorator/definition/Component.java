package com.demo.zyy.pattern.decorator.definition;

/**
 * 抽象组件
 */
public interface Component {
    void methodA();
}
