package com.demo.zyy.pattern.decorator.room;

/**
 * 测试
 */
public class DecoratorDesignPatternMain {
    public static void main(String args[]) {
        Room room = new CurtainDecorator(new ColorDecorator(new SimpleRoom()));
        System.out.println(room.showRoom());
    }
}
