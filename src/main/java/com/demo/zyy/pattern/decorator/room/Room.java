package com.demo.zyy.pattern.decorator.room;

/**
 * 房间
 */
public interface Room {
    String showRoom();
}
