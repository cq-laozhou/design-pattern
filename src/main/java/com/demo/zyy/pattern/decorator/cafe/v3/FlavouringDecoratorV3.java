package com.demo.zyy.pattern.decorator.cafe.v3;

/**
 * 调料装饰者抽象
 */
public abstract class FlavouringDecoratorV3 extends BeverageV3 {
    BeverageV3 beverage; //被装饰对象

    public FlavouringDecoratorV3(BeverageV3 beverage) {
        this.beverage = beverage;
    }

    public BeverageV3 getBeverage() {
        return beverage;
    }

    public void setBeverage(BeverageV3 beverage) {
        this.beverage = beverage;
    }

    @Override
    public String getDescription() {
        return "加" + super.getDescription() + ", " + beverage.getDescription();
    }
}
