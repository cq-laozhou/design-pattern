package com.demo.zyy.pattern.decorator.room;

/**
 * 毛坯房
 */
public class SimpleRoom implements Room{
    @Override
    public String showRoom() {
        return "毛坯房";
    }
}
