package com.demo.zyy.pattern.command.remotecontrol.v5;

/**
 * 打开空调指令
 */
public class AirConditionerPowerOnCommandV5 implements ControlCommandV5 {

    private AirConditionerV5 airConditionerV4;

    public AirConditionerPowerOnCommandV5(AirConditionerV5 airConditionerV4) {
        this.airConditionerV4 = airConditionerV4;
    }

    @Override
    public void execute() {
        System.out.println("执行【打开空调指令】...");
        airConditionerV4.turnOn();
    }

    @Override
    public String desc() {
        return "打开空调指令";
    }

    @Override
    public void undo() {
        airConditionerV4.turnOff();
    }
}
