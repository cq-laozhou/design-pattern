package com.demo.zyy.pattern.command.remotecontrol.v4;

/**
 * 遥控指令
 */
public interface ControlCommandV4 {
    void execute();
    String desc();
}
