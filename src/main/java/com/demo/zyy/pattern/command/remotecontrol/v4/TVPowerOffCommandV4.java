package com.demo.zyy.pattern.command.remotecontrol.v4;

import com.demo.zyy.pattern.command.remotecontrol.v3.TelevisionV3;

/**
 * 关闭电视指令
 */
public class TVPowerOffCommandV4 implements ControlCommandV4 {

    private TelevisionV4 television;

    public TVPowerOffCommandV4(TelevisionV4 television) {
        this.television = television;
    }

    @Override
    public void execute() {
        System.out.println("执行【关闭电视指令】...");
        television.off();
    }

    @Override
    public String desc() {
        return "关闭电视指令";
    }
}
