package com.demo.zyy.pattern.command.remotecontrol.v4;

/**
 * 空指令-什么都不做
 */
public class NoneCommandV4 implements ControlCommandV4 {

    @Override
    public void execute() {
    }

    @Override
    public String desc() {
        return "空指令";
    }
}
