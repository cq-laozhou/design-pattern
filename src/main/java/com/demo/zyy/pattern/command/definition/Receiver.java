package com.demo.zyy.pattern.command.definition;

/**
 * 接受者-也就是执行请求者
 */
public class Receiver {
    public void doSpecial(){
        System.out.println("接受者，实际执行请求....");
    }
}
