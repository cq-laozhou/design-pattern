package com.demo.zyy.pattern.command.remotecontrol.v5;

/**
 * 关闭电视指令
 */
public class TVPowerOffCommandV5 implements ControlCommandV5 {

    private TelevisionV5 television;

    public TVPowerOffCommandV5(TelevisionV5 television) {
        this.television = television;
    }

    @Override
    public void execute() {
        System.out.println("执行【关闭电视指令】...");
        television.off();
    }

    @Override
    public String desc() {
        return "关闭电视指令";
    }

    @Override
    public void undo() {
        television.on();
    }
}
