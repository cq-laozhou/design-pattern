package com.demo.zyy.pattern.command.remotecontrol.v6;

import com.demo.zyy.pattern.command.remotecontrol.v5.ControlCommandV5;

import java.util.List;

/**
 * 宏命令，组合其他命令
 */
public class MacroCommandV6 implements ControlCommandV5{

    private List<ControlCommandV5> commandV5s;

    public MacroCommandV6(List<ControlCommandV5> commandV5s) {
        this.commandV5s = commandV5s;
    }

    @Override
    public void execute() {
        commandV5s.forEach(command -> command.execute());
    }

    @Override
    public String desc() {
        return commandV5s.stream().map(controlCommandV5 -> controlCommandV5.desc())
                .reduce((s1, s2) -> s1 + "," + s2).get();
    }

    @Override
    public void undo() {
        commandV5s.forEach(command -> command.undo());
    }
}
