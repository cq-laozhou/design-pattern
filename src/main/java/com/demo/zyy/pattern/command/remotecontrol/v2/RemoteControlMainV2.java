package com.demo.zyy.pattern.command.remotecontrol.v2;

/**
 * 测试
 */
public class RemoteControlMainV2 {
    public static void main(String[] args) {
        RemoteControlV2 remoteControl = new RemoteControlV2();
        remoteControl.tvPowerOn();
        System.out.println();
        remoteControl.airConditionerPowerOn();
        System.out.println();
        remoteControl.tvPowerOff();
        System.out.println();
        remoteControl.airConditionerPowerOff();
    }
}
