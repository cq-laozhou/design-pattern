package com.demo.zyy.pattern.command.definition;

/**
 * 具体的命令
 */
public class ConcreteCommand implements Command {
    private Receiver receiver;

    public ConcreteCommand(Receiver receiver) {
        this.receiver = receiver;
    }

    @Override
    public void execute() {
        receiver.doSpecial();
    }
}
