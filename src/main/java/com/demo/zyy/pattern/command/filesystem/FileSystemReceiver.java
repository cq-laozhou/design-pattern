package com.demo.zyy.pattern.command.filesystem;

/**
 * 文件系统操作接口， 文件行为者
 */
public interface FileSystemReceiver {
    void openFile();
    void writeFile();
    void closeFile();
}
