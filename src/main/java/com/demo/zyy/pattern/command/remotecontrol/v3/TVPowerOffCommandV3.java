package com.demo.zyy.pattern.command.remotecontrol.v3;

/**
 * 关闭电视指令
 */
public class TVPowerOffCommandV3 implements ControlCommandV3 {

    private TelevisionV3 television;

    public TVPowerOffCommandV3(TelevisionV3 television) {
        this.television = television;
    }

    @Override
    public void execute() {
        System.out.println("执行【关闭电视指令】...");
        television.off();
    }
}
