package com.demo.zyy.pattern.command.remotecontrol.v2;

/**
 * 电视-小米
 */
public class TelevisionV2 {
    public void on(){
        System.out.println("打开电视....");
        System.out.println("正在放映<<哪吒>>");
    }

    public void off() {
        System.out.println("关闭电视....");
    }
}
