package com.demo.zyy.pattern.command.remotecontrol.v3;

/**
 * 空调-格力
 */
public class AirConditionerV3 {
    public void turnOn(){
        System.out.println("打开空调....");
        System.out.println("吹出26度的舒适冷风....");
    }

    public void turnOff() {
        System.out.println("关闭空调....");
    }
}
