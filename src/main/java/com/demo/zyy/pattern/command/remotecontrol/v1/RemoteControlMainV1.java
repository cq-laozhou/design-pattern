package com.demo.zyy.pattern.command.remotecontrol.v1;

/**
 * 测试
 */
public class RemoteControlMainV1 {
    public static void main(String[] args) {
        TVRemoteControlV1 remoteControl = new TVRemoteControlV1();
        remoteControl.powerOn();
        System.out.println();
        remoteControl.powerOff();
    }
}
