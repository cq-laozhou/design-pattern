package com.demo.zyy.pattern.command.remotecontrol.v5;

/**
 * 打开电视指令
 */
public class TVPowerOnCommandV5 implements ControlCommandV5 {

    private TelevisionV5 television;

    public TVPowerOnCommandV5(TelevisionV5 television) {
        this.television = television;
    }

    @Override
    public void execute() {
        System.out.println("执行【打开电视指令】...");
        television.on();
    }

    @Override
    public String desc() {
        return "打开电视指令";
    }

    @Override
    public void undo() {
        television.off();
    }
}
