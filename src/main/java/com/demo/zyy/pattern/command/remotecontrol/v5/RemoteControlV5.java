package com.demo.zyy.pattern.command.remotecontrol.v5;

/**
 * V5版遥控板 - 4个功能按键和1个撤销按键遥控板
 */
public class RemoteControlV5 {

    //4个按钮
    private ControlCommandV5[] commands = new ControlCommandV5[4];
    //撤销按钮
    private ControlCommandV5 undoCommand;

    public void setCommand(int index, ControlCommandV5 command){
        System.out.println("设置遥控板的第"+index+"个按钮，功能是"+command.desc());
        commands[index] = command;
    }

    public void pressDownButton(int index){
        System.out.println("按下遥控板的第"+index+"个按钮....");
        System.out.println("执行的功能是"+commands[index].desc());
        commands[index].execute();
        undoCommand = commands[index];
    }

    public void pressDownUndoButton(){
        System.out.println("按下遥控板的撤销按钮....");
        undoCommand.undo();
    }

}
