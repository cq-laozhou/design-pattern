package com.demo.zyy.pattern.command.remotecontrol.v4;

/**
 * 打开电视指令
 */
public class TVPowerOnCommandV4 implements ControlCommandV4 {

    private TelevisionV4 television;

    public TVPowerOnCommandV4(TelevisionV4 television) {
        this.television = television;
    }

    @Override
    public void execute() {
        System.out.println("执行【打开电视指令】...");
        television.on();
    }

    @Override
    public String desc() {
        return "打开电视指令";
    }
}
