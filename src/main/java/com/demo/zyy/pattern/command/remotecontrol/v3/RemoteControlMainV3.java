package com.demo.zyy.pattern.command.remotecontrol.v3;

/**
 * 测试
 */
public class RemoteControlMainV3 {
    public static void main(String[] args) {
        RemoteControlV3 remoteControl = new RemoteControlV3();
        remoteControl.tvPowerOn();
        System.out.println();
        remoteControl.airConditionerPowerOn();
        System.out.println();
        remoteControl.tvPowerOff();
        System.out.println();
        remoteControl.airConditionerPowerOff();
    }
}
