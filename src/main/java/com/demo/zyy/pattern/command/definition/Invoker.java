package com.demo.zyy.pattern.command.definition;

/**
 * 调用者-发起请求者
 */
public class Invoker {
    private Command command;

    public void setCommand(Command command) {
        this.command = command;
    }

    public void doSomething(){
        command.execute();
    }
}
