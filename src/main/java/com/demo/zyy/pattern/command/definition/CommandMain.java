package com.demo.zyy.pattern.command.definition;

/**
 * 客户
 */
public class CommandMain {
    public static void main(String[] args) {
        Invoker invoker = new Invoker();
        invoker.setCommand(new ConcreteCommand(new Receiver()));
        invoker.doSomething();

        invoker.setCommand(() -> {
            System.out.println("直接在命令里面就把事情做了...");
        });
        invoker.doSomething();
    }
}

