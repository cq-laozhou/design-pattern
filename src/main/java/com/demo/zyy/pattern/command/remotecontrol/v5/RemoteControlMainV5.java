package com.demo.zyy.pattern.command.remotecontrol.v5;

/**
 * 测试
 */
public class RemoteControlMainV5 {
    public static void main(String[] args) {
        TelevisionV5 television = new TelevisionV5();
        AirConditionerV5 airConditioner = new AirConditionerV5();

        RemoteControlV5 remoteControl = new RemoteControlV5();
        remoteControl.setCommand(0, new TVPowerOnCommandV5(television));
        remoteControl.setCommand(1, new TVPowerOffCommandV5(television));
        remoteControl.setCommand(2, new AirConditionerPowerOnCommandV5(airConditioner));
        remoteControl.setCommand(3, new AirConditionerPowerOffCommandV5(airConditioner));

        System.out.println();
        remoteControl.pressDownButton(0);
        System.out.println();
        remoteControl.pressDownButton(1);
        System.out.println();
        remoteControl.pressDownUndoButton();
        System.out.println();
        remoteControl.pressDownButton(2);
        System.out.println();
        remoteControl.pressDownButton(3);
        System.out.println();
        remoteControl.pressDownUndoButton();

    }
}
