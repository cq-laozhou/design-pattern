package com.demo.zyy.pattern.command.filesystem;

/**
 * 调用者
 */
public class FileInvoker {

    public Command command;

    public FileInvoker(Command command) {
        this.command = command;
    }

    public void execute(){
        command.execute();
    }
}
