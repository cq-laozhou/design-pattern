package com.demo.zyy.pattern.command.filesystem;

/**
 * 命令抽象
 */
public interface Command {
    void execute();
}
