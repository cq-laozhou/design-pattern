package com.demo.zyy.pattern.command.remotecontrol.v4;

/**
 * V4版遥控板 - 4个按键的遥控板
 */
public class RemoteControlV4 {

    //4个按钮
    private ControlCommandV4[] commands = new ControlCommandV4[4];

    public void setCommand(int index, ControlCommandV4 command){
        System.out.println("设置遥控板的第"+index+"个按钮，功能是"+command.desc());
        commands[index] = command;
    }

    public void pressDownButton(int index){
        System.out.println("按下遥控板的第"+index+"个按钮....");
        System.out.println("执行的功能是"+commands[index].desc());
        commands[index].execute();
    }

}
