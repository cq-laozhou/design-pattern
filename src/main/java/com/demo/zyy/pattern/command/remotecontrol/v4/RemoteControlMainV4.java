package com.demo.zyy.pattern.command.remotecontrol.v4;

/**
 * 测试
 */
public class RemoteControlMainV4 {
    public static void main(String[] args) {
        TelevisionV4 television = new TelevisionV4();
        AirConditionerV4 airConditioner = new AirConditionerV4();

        RemoteControlV4 remoteControl = new RemoteControlV4();
        remoteControl.setCommand(0, new TVPowerOnCommandV4(television));
        remoteControl.setCommand(1, new TVPowerOffCommandV4(television));
        remoteControl.setCommand(2, new AirConditionerPowerOnCommandV4(airConditioner));
        remoteControl.setCommand(3, new AirConditionerPowerOffCommandV4(airConditioner));

        System.out.println();
        remoteControl.pressDownButton(0);
        System.out.println();
        remoteControl.pressDownButton(1);
        System.out.println();
        remoteControl.pressDownButton(2);
        System.out.println();
        remoteControl.pressDownButton(3);

    }
}
