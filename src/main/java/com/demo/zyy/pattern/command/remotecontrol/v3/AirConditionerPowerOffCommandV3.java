package com.demo.zyy.pattern.command.remotecontrol.v3;

/**
 * 关闭空调指令
 */
public class AirConditionerPowerOffCommandV3 implements ControlCommandV3 {

    private AirConditionerV3 airConditionerV3;

    public AirConditionerPowerOffCommandV3(AirConditionerV3 airConditionerV3) {
        this.airConditionerV3 = airConditionerV3;
    }

    @Override
    public void execute() {
        System.out.println("执行【关闭空调指令】...");
        airConditionerV3.turnOff();
    }
}
