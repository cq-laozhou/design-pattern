package com.demo.zyy.pattern.command.remotecontrol.v3;

/**
 * 遥控指令
 */
public interface ControlCommandV3 {
    void execute();
}
