package com.demo.zyy.pattern.command.remotecontrol.v3;

/**
 * V3版遥控板
 */
public class RemoteControlV3 {

    private TVPowerOnCommandV3 tvPowerOnCommand;
    private TVPowerOffCommandV3 tvPowerOffCommand;
    private AirConditionerPowerOnCommandV3 airConditionerPowerOnCommand;
    private AirConditionerPowerOffCommandV3 airConditionerPowerOffCommand;

    public RemoteControlV3() {
        TelevisionV3 televisionV3 = new TelevisionV3();
        tvPowerOnCommand = new TVPowerOnCommandV3(televisionV3);
        tvPowerOffCommand = new TVPowerOffCommandV3(televisionV3);

        AirConditionerV3 airConditionerV3 = new AirConditionerV3();
        airConditionerPowerOnCommand = new AirConditionerPowerOnCommandV3(airConditionerV3);
        airConditionerPowerOffCommand = new AirConditionerPowerOffCommandV3(airConditionerV3);
    }

    public void tvPowerOn(){
        System.out.println("按下遥控板的电视电源打开按钮....");
        tvPowerOnCommand.execute();
    }

    public void tvPowerOff(){
        System.out.println("按下遥控板的电视电源关闭按钮....");
        tvPowerOffCommand.execute();
    }

    public void airConditionerPowerOn(){
        System.out.println("按下遥控板的空调电源打开按钮....");
        airConditionerPowerOnCommand.execute();
    }

    public void airConditionerPowerOff(){
        System.out.println("按下遥控板的空调电源关闭按钮....");
        airConditionerPowerOffCommand.execute();
    }
}
