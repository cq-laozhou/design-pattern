package com.demo.zyy.pattern.command.remotecontrol.v3;

/**
 * 打开电视指令
 */
public class TVPowerOnCommandV3 implements ControlCommandV3 {

    private TelevisionV3 television;

    public TVPowerOnCommandV3(TelevisionV3 television) {
        this.television = television;
    }

    @Override
    public void execute() {
        System.out.println("执行【打开电视指令】...");
        television.on();
    }
}
