package com.demo.zyy.pattern.command.remotecontrol.v1;

/**
 * V1版遥控板
 */
public class TVRemoteControlV1 {

    TelevisionV1 television;

    public TVRemoteControlV1() {
        television = new TelevisionV1();
    }

    public void powerOn(){
        System.out.println("按下遥控板的电源打开按钮....");
        television.on();
    }

    public void powerOff(){
        System.out.println("按下遥控板的电源关闭按钮....");
        television.off();
    }
}
