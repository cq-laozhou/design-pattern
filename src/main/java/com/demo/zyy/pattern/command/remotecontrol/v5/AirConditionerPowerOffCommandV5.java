package com.demo.zyy.pattern.command.remotecontrol.v5;

/**
 * 关闭空调指令
 */
public class AirConditionerPowerOffCommandV5 implements ControlCommandV5 {

    private AirConditionerV5 airConditionerV4;


    public AirConditionerPowerOffCommandV5(AirConditionerV5 airConditionerV4) {
        this.airConditionerV4 = airConditionerV4;
    }

    @Override
    public void execute() {
        System.out.println("执行【关闭空调指令】...");
        airConditionerV4.turnOff();
    }

    @Override
    public String desc() {
        return "关闭空调指令";
    }

    @Override
    public void undo() {
        airConditionerV4.turnOn();
    }
}
