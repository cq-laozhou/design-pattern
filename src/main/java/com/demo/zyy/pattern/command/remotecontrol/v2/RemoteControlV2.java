package com.demo.zyy.pattern.command.remotecontrol.v2;

/**
 * V2版遥控板
 */
public class RemoteControlV2 {

    TelevisionV2 television;
    AirConditionerV2 airConditioner;

    public RemoteControlV2() {
        television = new TelevisionV2();
        airConditioner = new AirConditionerV2();
    }

    public void tvPowerOn(){
        System.out.println("按下遥控板的电视电源打开按钮....");
        television.on();
    }

    public void tvPowerOff(){
        System.out.println("按下遥控板的电视电源关闭按钮....");
        television.off();
    }

    public void airConditionerPowerOn(){
        System.out.println("按下遥控板的空调电源打开按钮....");
        airConditioner.turnOn();
    }

    public void airConditionerPowerOff(){
        System.out.println("按下遥控板的空调电源关闭按钮....");
        airConditioner.turnOff();
    }
}

