package com.demo.zyy.pattern.command.definition;

/**
 * 命令接口
 */
public interface Command {
    void execute();
}
