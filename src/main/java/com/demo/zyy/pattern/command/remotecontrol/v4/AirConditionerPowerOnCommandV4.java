package com.demo.zyy.pattern.command.remotecontrol.v4;

/**
 * 打开空调指令
 */
public class AirConditionerPowerOnCommandV4 implements ControlCommandV4 {

    private AirConditionerV4 airConditionerV4;

    public AirConditionerPowerOnCommandV4(AirConditionerV4 airConditionerV4) {
        this.airConditionerV4 = airConditionerV4;
    }

    @Override
    public void execute() {
        System.out.println("执行【打开空调指令】...");
        airConditionerV4.turnOn();
    }

    @Override
    public String desc() {
        return "打开空调指令";
    }
}
