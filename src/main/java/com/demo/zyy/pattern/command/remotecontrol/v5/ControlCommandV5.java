package com.demo.zyy.pattern.command.remotecontrol.v5;

/**
 * 遥控指令
 */
public interface ControlCommandV5 {
    void execute();//执行
    String desc();//描述
    void undo(); //撤销
}
