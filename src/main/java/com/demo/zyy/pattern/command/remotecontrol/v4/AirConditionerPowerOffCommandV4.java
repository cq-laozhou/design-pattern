package com.demo.zyy.pattern.command.remotecontrol.v4;

/**
 * 关闭空调指令
 */
public class AirConditionerPowerOffCommandV4 implements ControlCommandV4 {

    private AirConditionerV4 airConditionerV4;

    public AirConditionerPowerOffCommandV4(AirConditionerV4 airConditionerV4) {
        this.airConditionerV4 = airConditionerV4;
    }

    @Override
    public void execute() {
        System.out.println("执行【关闭空调指令】...");
        airConditionerV4.turnOff();
    }

    @Override
    public String desc() {
        return "关闭空调指令";
    }
}
