package com.demo.zyy.pattern.status.tvremote.v2;

/**
 * 关机状态
 */
public class TVStopState implements TvState {
    private TVContext context;

    public TVStopState(TVContext context) {
        this.context = context;
    }

    @Override
    public void pressPowerButton() {
        System.out.println("TV is turned ON");
        context.setCurrentState(context.getOnState());
    }
}
