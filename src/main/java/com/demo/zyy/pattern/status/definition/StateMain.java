package com.demo.zyy.pattern.status.definition;

/**
 * 测试
 */
public class StateMain {
    public static void main(String[] args) {
        Context context = new Context();

        context.request1();
        context.request2();
        context.request3();

    }
}
