package com.demo.zyy.pattern.status.gumballmachine.v2;

import com.demo.zyy.pattern.status.gumballmachine.v1.GumballMachineV1;

/**
 * 自助售货机测试
 */
public class GumballMachineMainV2 {
    public static void main(String[] args) {
        GumballMachineV2 machine = new GumballMachineV2(3);

        System.out.println(machine);

        machine.insertQuarter();
        machine.turnCrank();

        System.out.println(machine);

        machine.insertQuarter();
        machine.ejectQuarter();
        machine.turnCrank();

        System.out.println(machine);

        machine.insertQuarter();
        machine.turnCrank();
        machine.insertQuarter();
        machine.turnCrank();

        System.out.println(machine);
        machine.insertQuarter();
    }
}
