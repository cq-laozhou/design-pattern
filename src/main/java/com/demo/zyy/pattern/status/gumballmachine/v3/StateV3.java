package com.demo.zyy.pattern.status.gumballmachine.v3;

/**
 * 状态接口
 *  定义自助售货机的所有方法,每种状态都有一种实现，分别实现在该种状态下的行为。
 */
public interface StateV3 {

    void insertQuarter();

    void ejectQuarter();

    void turnCrank();

    void dispense();
}
