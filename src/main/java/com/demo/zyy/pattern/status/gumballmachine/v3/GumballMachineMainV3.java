package com.demo.zyy.pattern.status.gumballmachine.v3;

import com.demo.zyy.pattern.status.gumballmachine.v2.GumballMachineV2;

/**
 * 自助售货机测试
 */
public class GumballMachineMainV3 {
    public static void main(String[] args) {
        GumballMachineV3 machine = new GumballMachineV3(3);

        System.out.println(machine);

        machine.insertQuarter();
        machine.turnCrank();

        System.out.println(machine);

        machine.insertQuarter();
        machine.ejectQuarter();
        machine.turnCrank();

        System.out.println(machine);

        machine.insertQuarter();
        machine.turnCrank();
        machine.insertQuarter();
        machine.turnCrank();

        System.out.println(machine);
        machine.insertQuarter();
    }
}
