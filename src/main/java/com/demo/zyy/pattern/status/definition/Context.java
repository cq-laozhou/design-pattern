package com.demo.zyy.pattern.status.definition;

/**
 * 状态上下文
 */
public class Context {

    private State stateA ;
    private State stateB ;
    private State stateC ;

    private State state;

    public Context() {
        stateA = new ConcreteStateA(this);
        stateB = new ConcreteStateB(this);
        stateC = new ConcreteStateC(this);

        this.state = stateA;
    }

    public void request1(){
        state.action1();
    }

    public void request2(){
        state.action2();
    }

    public void request3(){
        state.action3();
    }

    public void changeState(State state){
        this.state = state;
    }

    public State getStateA() {
        return stateA;
    }

    public State getStateB() {
        return stateB;
    }

    public State getStateC() {
        return stateC;
    }

    public State getState() {
        return state;
    }
}
