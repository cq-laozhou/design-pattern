package com.demo.zyy.pattern.status.tvremote.v2;

/**
 * 电视机
 */
public class TVContext {

    private TvState onState ;
    private TvState offState;

    private TvState currentState;

    public TVContext() {
        onState = new TVStartState(this);
        offState = new TVStopState(this);
        currentState = offState;
    }

    public void pressPowerButton(){
        currentState.pressPowerButton();
    }

    public TvState getOnState() {
        return onState;
    }

    public void setOnState(TvState onState) {
        this.onState = onState;
    }

    public TvState getOffState() {
        return offState;
    }

    public void setOffState(TvState offState) {
        this.offState = offState;
    }

    public TvState getCurrentState() {
        return currentState;
    }

    public void setCurrentState(TvState currentState) {
        this.currentState = currentState;
    }
}
