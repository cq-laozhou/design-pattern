package com.demo.zyy.pattern.status.definition;

/**
 * 状态C
 */
public class ConcreteStateC implements State {
    private Context context;

    public ConcreteStateC(Context context) {
        this.context = context;
    }

    @Override
    public void action1() {

    }

    @Override
    public void action2() {

    }

    @Override
    public void action3() {
        System.out.println("状态C时，action3动作...状态变更到A");
        context.changeState(context.getStateA());
    }
}
