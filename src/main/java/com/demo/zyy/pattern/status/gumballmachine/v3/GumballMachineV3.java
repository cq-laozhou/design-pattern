package com.demo.zyy.pattern.status.gumballmachine.v3;

/**
 * 自助售货机
 */
public class GumballMachineV3 {

    private StateV3 saleOut       ;    //售罄
    private StateV3 noQuarter     ;    //没有硬币
    private StateV3 hasQuarter    ;    //有硬币
    private StateV3 sold           ;    //出售中
    private StateV3 win           ;    //中奖

    private StateV3 state;
    private int count;

    public GumballMachineV3(int count) {
        saleOut = new SoldOutStateV3(this);
        noQuarter = new NoQuarterStateV3(this);
        hasQuarter = new HasQuarterStateV3(this);
        sold = new SoldStateV3(this);
        win = new WinStateV3(this);
        this.count = count;
        if(this.count > 0){
            state = noQuarter;
        }

    }

    //投入硬币的动作
    public void insertQuarter(){
        state.insertQuarter();
    }

    //退钱的动作
    public void ejectQuarter(){
        state.ejectQuarter();
    }

    //按下购买按钮（转动曲柄）
    public void turnCrank(){
        state.turnCrank();
        dispense();
    }

    //发放糖果（机器内部自动触发）
    public void dispense(){
        state.dispense();
    }

    //状态变更。
    public void changeState(StateV3 newState){
        state = newState;
    }

    //减库存
    public void countDEC(){
        this.count--;
    }

    public StateV3 getSaleOut() {
        return saleOut;
    }

    public void setSaleOut(StateV3 saleOut) {
        this.saleOut = saleOut;
    }

    public StateV3 getNoQuarter() {
        return noQuarter;
    }

    public void setNoQuarter(StateV3 noQuarter) {
        this.noQuarter = noQuarter;
    }

    public StateV3 getHasQuarter() {
        return hasQuarter;
    }

    public void setHasQuarter(StateV3 hasQuarter) {
        this.hasQuarter = hasQuarter;
    }

    public StateV3 getSold() {
        return sold;
    }

    public void setSold(StateV3 sold) {
        this.sold = sold;
    }

    public StateV3 getState() {
        return state;
    }

    public void setState(StateV3 state) {
        this.state = state;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public StateV3 getWin() {
        return win;
    }

    public void setWin(StateV3 win) {
        this.win = win;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("GumballMachineV3{");
        sb.append("state=").append(state);
        sb.append(", count=").append(count);
        sb.append('}');
        return sb.toString();
    }
}


