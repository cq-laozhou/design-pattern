package com.demo.zyy.pattern.status.gumballmachine.v3;

/**
 * 中奖状态
 */
public class WinStateV3 implements StateV3 {
    GumballMachineV3 machine;

    public WinStateV3(GumballMachineV3 machine) {
        this.machine = machine;
    }

    @Override
    public void insertQuarter() {
        System.out.println("请稍等，正在出糖果。。。。");
    }

    @Override
    public void ejectQuarter() {
        System.out.println("正在出糖果中，不能退钱");
    }

    @Override
    public void turnCrank() {
        System.out.println("正在出糖果中，等着吧你。");
    }

    @Override
    public void dispense() {
        System.out.println("恭喜你，你中奖了买一送一。。");
        oneGumball();
        if (machine.getState() != machine.getSaleOut()){
            oneGumball();
        }
    }

    private void oneGumball() {
        System.out.println("来，给你糖果。。");
        //状态变化
        machine.countDEC();
        if(machine.getCount() > 0){
            machine.changeState(machine.getNoQuarter());
        }else{
            machine.changeState(machine.getSaleOut());
        }
    }
}
