package com.demo.zyy.pattern.status.gumballmachine.v2;

/**
 * 自助售货机
 */
public class GumballMachineV2 {

    private StateV2 saleOut       ;    //售罄
    private StateV2 noQuarter     ;    //没有硬币
    private StateV2 hasQuarter    ;    //有硬币
    private StateV2 sold           ;    //出售中

    private StateV2 state;
    private int count;

    public GumballMachineV2(int count) {
        saleOut = new SoldOutStateV2(this);
        noQuarter = new NoQuarterStateV2(this);
        hasQuarter = new HasQuarterStateV2(this);
        sold = new SoldStateV2(this);
        this.count = count;
        if(this.count > 0){
            state = noQuarter;
        }

    }

    //投入硬币的动作
    public void insertQuarter(){
        state.insertQuarter();
    }

    //退钱的动作
    public void ejectQuarter(){
        state.ejectQuarter();
    }

    //按下购买按钮（转动曲柄）
    public void turnCrank(){
        state.turnCrank();
        dispense();
    }

    //发放糖果（机器内部自动触发）
    public void dispense(){
        state.dispense();
    }

    //状态变更。
    public void changeState(StateV2 newState){
        state = newState;
    }

    //减库存
    public void countDEC(){
        this.count--;
    }

    public StateV2 getSaleOut() {
        return saleOut;
    }

    public void setSaleOut(StateV2 saleOut) {
        this.saleOut = saleOut;
    }

    public StateV2 getNoQuarter() {
        return noQuarter;
    }

    public void setNoQuarter(StateV2 noQuarter) {
        this.noQuarter = noQuarter;
    }

    public StateV2 getHasQuarter() {
        return hasQuarter;
    }

    public void setHasQuarter(StateV2 hasQuarter) {
        this.hasQuarter = hasQuarter;
    }

    public StateV2 getSold() {
        return sold;
    }

    public void setSold(StateV2 sold) {
        this.sold = sold;
    }

    public StateV2 getState() {
        return state;
    }

    public void setState(StateV2 state) {
        this.state = state;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }



    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("GumballMachineV2{");
        sb.append(", state=").append(state);
        sb.append(", count=").append(count);
        sb.append('}');
        return sb.toString();
    }
}


