package com.demo.zyy.pattern.status.gumballmachine.v3;

import java.util.Random;

/**
 * 有币状态
 */
public class HasQuarterStateV3 implements StateV3 {
    GumballMachineV3 machine;
    Random random;

    public HasQuarterStateV3(GumballMachineV3 machine) {
        this.machine = machine;
        this.random = new Random();
    }

    @Override
    public void insertQuarter() {
        System.out.println("你已经投入硬币，不需要继续投入");
    }

    @Override
    public void ejectQuarter() {
        //状态变更
        System.out.println("退钱给你了");
        machine.changeState(machine.getNoQuarter());
    }

    @Override
    public void turnCrank() {
        //状态变化
        System.out.println("接受转动,出糖中...");
        int winRand = random.nextInt(10);
        System.out.println("---"+winRand);
        if(winRand == 0){
            machine.changeState(machine.getWin());
        }else{
            machine.changeState(machine.getSold());
        }

    }

    @Override
    public void dispense() {
        System.out.println("先转动下曲柄，才能给你糖果哦");
    }
}
