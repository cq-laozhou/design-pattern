package com.demo.zyy.pattern.status.gumballmachine.v2;

/**
 * 没币状态
 */
public class NoQuarterStateV2 implements StateV2 {
    GumballMachineV2 machine;

    public NoQuarterStateV2(GumballMachineV2 machine) {
        this.machine = machine;
    }

    @Override
    public void insertQuarter() {
        System.out.println("你投入了硬币");
        //状态变更
        machine.changeState(machine.getHasQuarter());
    }

    @Override
    public void ejectQuarter() {
        System.out.println("没有投入硬币，退不了");
    }

    @Override
    public void turnCrank() {
        System.out.println("还没有投入钱，转动也没用");
    }

    @Override
    public void dispense() {
        System.out.println("先付钱，才能给糖果");
    }
}
