package com.demo.zyy.pattern.status.tvremote.v2;

/**
 * 测试
 */
public class TVRemote {
    public static void main(String[] args) {
        TVContext context = new TVContext();
        context.pressPowerButton();
        context.pressPowerButton();
        context.pressPowerButton();
    }
}
