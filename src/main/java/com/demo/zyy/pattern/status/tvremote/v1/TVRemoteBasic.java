package com.demo.zyy.pattern.status.tvremote.v1;

/**
 * 通过IF-ELSE来实现基于状态的不同行为。
 */
public class TVRemoteBasic {
    private String state="OFF";

    public void setState(String state){
        this.state=state;
    }

    public void pressPowerButton(){
        if(state.equalsIgnoreCase("ON")){
            System.out.println("TV is turned OFF");
            setState("OFF");
        }else if(state.equalsIgnoreCase("OFF")){
            System.out.println("TV is turned ON");
            setState("ON");
        }
    }

    public static void main(String args[]){
        TVRemoteBasic remote = new TVRemoteBasic();
        remote.pressPowerButton();
        remote.pressPowerButton();
    }
}
