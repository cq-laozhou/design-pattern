package com.demo.zyy.pattern.status.definition;

/**
 * 状态接口
 */
public interface State {
    void action1();
    void action2();
    void action3();
}
