package com.demo.zyy.pattern.status.definition;

/**
 * 状态B
 */
public class ConcreteStateB implements State {
    private Context context;

    public ConcreteStateB(Context context) {
        this.context = context;
    }
    @Override
    public void action1() {
    }

    @Override
    public void action2() {
        System.out.println("状态B时，action2动作...状态变更到C");
        context.changeState(context.getStateC());
    }

    @Override
    public void action3() {

    }
}
