package com.demo.zyy.pattern.status.definition;

/**
 * 状态A
 */
public class ConcreteStateA implements State {
    private Context context;

    public ConcreteStateA(Context context) {
        this.context = context;
    }

    @Override
    public void action1() {
        System.out.println("状态A时，action1动作,状态切换到B...");
        context.changeState(context.getStateB());
    }

    @Override
    public void action2() {
    }

    @Override
    public void action3() {
    }
}
