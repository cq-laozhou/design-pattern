package com.demo.zyy.pattern.status.tvremote.v2;

/**
 * 电视机状态
 */
public interface TvState {
    void pressPowerButton();
}
