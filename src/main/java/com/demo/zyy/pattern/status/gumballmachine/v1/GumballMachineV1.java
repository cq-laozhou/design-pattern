package com.demo.zyy.pattern.status.gumballmachine.v1;

/**
 * 自助售货机
 */
public class GumballMachineV1 {

    final static int SALE_OUT       = 0;    //售罄
    final static int NO_QUARTER     = 1;    //没有硬币
    final static int HAS_QUARTER    = 2;    //有硬币
    final static int SOLD           = 3;    //出售中

    private int state = SALE_OUT;
    private int count;

    public GumballMachineV1(int count) {
        this.count = count;
        if(this.count > 0){
            state = NO_QUARTER;
        }
    }

    //投入硬币的动作
    public void insertQuarter(){
        if(state == NO_QUARTER){
            //状态变化
            state = HAS_QUARTER;
            System.out.println("你投入了硬币");
        }else if(state == SALE_OUT){
            System.out.println("售罄状态下，不能投入硬币");
        }else if(state == HAS_QUARTER){
            System.out.println("你已经投入硬币，不需要继续投入");
        }else if (state == SOLD){
            System.out.println("请稍等，正在出糖果");
        }else{
            System.out.println("未知状态，怕是出错比较好。");
        }
    }

    //退钱的动作
    public void ejectQuarter(){
        if(state == NO_QUARTER){
            System.out.println("没有投入硬币，退不了");
        }else if(state == SALE_OUT){
            System.out.println("售罄状态下，不能投入硬币，当然也不能退钱了");
        }else if(state == HAS_QUARTER){
            //状态变化
            state = NO_QUARTER;
            System.out.println("退钱给你了");
        }else if (state == SOLD){
            System.out.println("正在出糖果中，不能退钱");
        }else{
            System.out.println("未知状态，怕是出错比较好。");
        }
    }

    //按下购买按钮（转动曲柄）
    public void turnCrank(){
        if(state == NO_QUARTER){
            System.out.println("还没有投入钱，转动也没用");
        }else if(state == SALE_OUT){
            System.out.println("售罄状态下，不能投入硬币，转动没用");
        }else if(state == HAS_QUARTER){
            //状态变化
            state = SOLD;
            System.out.println("接受转动,出糖中...");
            dispense();
        }else if (state == SOLD){
            System.out.println("正在出糖果中，等着吧你。");
        }else{
            System.out.println("未知状态，怕是出错比较好。");
        }
    }

    //发放糖果（机器内部自动触发）
    public void dispense(){
        if(state == NO_QUARTER){
            System.out.println("先付钱，才能给糖果");
        }else if(state == SALE_OUT){
            System.out.println("都没糖果了，给不了..");
        }else if(state == HAS_QUARTER){
            System.out.println("先转动下曲柄，才能给你糖果哦");
        }else if (state == SOLD){
            System.out.println("来，给你糖果。。");
            //状态变化
            count--;
            if(count > 0){
                state = NO_QUARTER;
            }else{
                state = SALE_OUT;
            }
        }else{
            System.out.println("未知状态，怕是出错比较好。");
        }
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("GumballMachineV1{");
        sb.append("state=").append(state);
        sb.append(", count=").append(count);
        sb.append('}');
        return sb.toString();
    }
}


