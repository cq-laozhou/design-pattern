package com.demo.zyy.pattern.status.gumballmachine.v2;

/**
 * 售罄状态
 */
public class SoldOutStateV2 implements StateV2 {
    GumballMachineV2 machine;

    public SoldOutStateV2(GumballMachineV2 machine) {
        this.machine = machine;
    }

    @Override
    public void insertQuarter() {
        System.out.println("售罄状态下，不能投入硬币");
    }

    @Override
    public void ejectQuarter() {
        System.out.println("售罄状态下，不能投入硬币，当然也不能退钱了");
    }

    @Override
    public void turnCrank() {
        System.out.println("售罄状态下，不能投入硬币，转动没用");
    }

    @Override
    public void dispense() {
        System.out.println("都没糖果了，给不了..");
    }
}
