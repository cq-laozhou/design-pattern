package com.demo.zyy.pattern.status.gumballmachine.v1;

/**
 * 自助售货机测试
 */
public class GumballMachineMainV1 {
    public static void main(String[] args) {
        GumballMachineV1 machine = new GumballMachineV1(3);

        System.out.println(machine);

        machine.insertQuarter();
        machine.turnCrank();

        System.out.println(machine);

        machine.insertQuarter();
        machine.ejectQuarter();
        machine.turnCrank();

        System.out.println(machine);

        machine.insertQuarter();
        machine.turnCrank();
        machine.insertQuarter();
        machine.turnCrank();

        System.out.println(machine);
        machine.insertQuarter();
    }
}
