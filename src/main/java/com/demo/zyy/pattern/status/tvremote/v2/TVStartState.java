package com.demo.zyy.pattern.status.tvremote.v2;

/**
 * 开机状态
 */
public class TVStartState implements TvState {

    private TVContext context;

    public TVStartState(TVContext context) {
        this.context = context;
    }

    @Override
    public void pressPowerButton() {
        System.out.println("TV is turned OFF");
        context.setCurrentState(context.getOffState());
    }
}
