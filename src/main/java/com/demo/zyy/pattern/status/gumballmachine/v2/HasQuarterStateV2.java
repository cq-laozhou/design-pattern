package com.demo.zyy.pattern.status.gumballmachine.v2;

/**
 * 有币状态
 */
public class HasQuarterStateV2 implements StateV2 {
    GumballMachineV2 machine;

    public HasQuarterStateV2(GumballMachineV2 machine) {
        this.machine = machine;
    }

    @Override
    public void insertQuarter() {
        System.out.println("你已经投入硬币，不需要继续投入");

    }

    @Override
    public void ejectQuarter() {
        //状态变更
        System.out.println("退钱给你了");
        machine.changeState(machine.getNoQuarter());
    }

    @Override
    public void turnCrank() {
        //状态变化
        System.out.println("接受转动,出糖中...");
        machine.changeState(machine.getSold());
    }

    @Override
    public void dispense() {
        System.out.println("先转动下曲柄，才能给你糖果哦");
    }
}
