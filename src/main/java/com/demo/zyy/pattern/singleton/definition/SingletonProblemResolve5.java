package com.demo.zyy.pattern.singleton.definition;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * 经典单例-多线程问题，解决方式5
 * 枚举实现
 */
public enum  SingletonProblemResolve5 {

    INSTANCE;

    public void doSomething(){
        System.out.println(hashCode());
    }

    public static void main(String[] args) throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        CountDownLatch mainWait = new CountDownLatch(10);
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                try {
                    latch.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                SingletonProblemResolve5.INSTANCE.doSomething();
                mainWait.countDown();

            }).start();
        }
        latch.countDown();
        mainWait.await(10 , TimeUnit.SECONDS);
    }
}
