package com.demo.zyy.pattern.singleton.chocolate.v2;

/**
 * 测试
 */
public class ChocolateMainV2 {
    public static void main(String[] args) {
        ChocolateBoilerV2 boiler = ChocolateBoilerV2.getInstance();
        boiler.fill();

        ChocolateBoilerV2 boiler2 = ChocolateBoilerV2.getInstance();
        boiler2.fill();

        System.out.println(boiler == boiler2);
    }
}
