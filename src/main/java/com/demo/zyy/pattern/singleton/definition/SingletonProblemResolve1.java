package com.demo.zyy.pattern.singleton.definition;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * 经典单例-多线程问题，解决方式1
 */
public class SingletonProblemResolve1 {

    private static SingletonProblemResolve1 uniqueInstance;

    private SingletonProblemResolve1() {
        System.out.println("创建了新的实例.....");
    }

    public static synchronized SingletonProblemResolve1 getInstance(){
        if(null == uniqueInstance){
            uniqueInstance = new SingletonProblemResolve1();
        }
        return uniqueInstance;
    }

    public static void main(String[] args) throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        CountDownLatch mainWait = new CountDownLatch(10);
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                try {
                    latch.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                SingletonProblemResolve1.getInstance();
                mainWait.countDown();

            }).start();
        }
        latch.countDown();
        mainWait.await(10 , TimeUnit.SECONDS);
    }
}
