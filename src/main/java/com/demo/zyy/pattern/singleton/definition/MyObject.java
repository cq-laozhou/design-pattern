package com.demo.zyy.pattern.singleton.definition;

/**
 * 私有的构造方法
 */
public class MyObject {
    private MyObject() {
    }

    public static MyObject getInstance(){
        return new MyObject();
    }
}
