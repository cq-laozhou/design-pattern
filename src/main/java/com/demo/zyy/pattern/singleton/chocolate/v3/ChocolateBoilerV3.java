package com.demo.zyy.pattern.singleton.chocolate.v3;

/**
 * 巧克力锅炉-双重检查加锁单例版
 */
public class ChocolateBoilerV3 {

    private static volatile ChocolateBoilerV3 boilerV2;

    private boolean empty;
    private boolean boiled;

    private ChocolateBoilerV3() {
        this.empty = true;
        this.boiled = false;
    }

    public static ChocolateBoilerV3 getInstance(){
        if(boilerV2 == null){
            synchronized (ChocolateBoilerV3.class) {
                if(boilerV2 == null) {
                    boilerV2 = new ChocolateBoilerV3();
                }
            }
        }
        return boilerV2;
    }

    public void fill(){
        System.out.println("尝试填充");
        if(isEmpty()){
            empty = false;
            boiled = false;
            System.out.println("执行填充。。。。");
        }
    }

    public void drain(){
        System.out.println("尝试排出");
        if(!isEmpty() && isBoiled()){
            empty = true;
            System.out.println("执行排出。。。。。");
        }
    }

    public void boil(){
        System.out.println("尝试煮沸");
        if(!isEmpty() && !isBoiled()){
            boiled = true;
            System.out.println("执行煮沸。。。。。");
        }
    }

    public boolean isEmpty() {
        return empty;
    }

    public boolean isBoiled() {
        return boiled;
    }
}
