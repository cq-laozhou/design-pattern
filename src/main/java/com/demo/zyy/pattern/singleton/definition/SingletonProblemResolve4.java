package com.demo.zyy.pattern.singleton.definition;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * 经典单例-多线程问题，解决方式4
 * 比尔.普夫单例
 */
public class SingletonProblemResolve4 {

    private SingletonProblemResolve4() {
        System.out.println("创建了新的实例.....");
    }

    public static SingletonProblemResolve4 getInstance(){
        return SingletonHolder.SINGLETON_PROBLEM_RESOLVE_4;
    }

    private static class SingletonHolder{
        private static final SingletonProblemResolve4 SINGLETON_PROBLEM_RESOLVE_4
                = new SingletonProblemResolve4();
    }

    public static void main(String[] args) throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        CountDownLatch mainWait = new CountDownLatch(10);
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                try {
                    latch.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                SingletonProblemResolve4.getInstance();
                mainWait.countDown();

            }).start();
        }
        latch.countDown();
        mainWait.await(10 , TimeUnit.SECONDS);
    }
}
