package com.demo.zyy.pattern.singleton.chocolate.v3;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * 测试
 */
public class ChocolateMainV3 {
    public static void main(String[] args) throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        CountDownLatch mainWait = new CountDownLatch(10);
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                try {
                    latch.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                ChocolateBoilerV3 boilerV3 = ChocolateBoilerV3.getInstance();
                boilerV3.fill();
                mainWait.countDown();

            }).start();
        }
        latch.countDown();
        mainWait.await(10 , TimeUnit.SECONDS);
    }
}
