package com.demo.zyy.pattern.singleton.chocolate.v1;

/**
 * 测试
 */
public class ChocolateMainV1 {
    public static void main(String[] args) {
        ChocolateBoilerV1 boiler = new ChocolateBoilerV1();
        boiler.fill();
        boiler.fill();
        boiler.boil();
        boiler.boil();
        boiler.drain();
        boiler.drain();

        ChocolateBoilerV1 boiler2 = new ChocolateBoilerV1();
        boiler.fill();
        boiler2.fill();
    }
}
