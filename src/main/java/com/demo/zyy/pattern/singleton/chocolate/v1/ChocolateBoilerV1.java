package com.demo.zyy.pattern.singleton.chocolate.v1;

/**
 * 巧克力锅炉
 */
public class ChocolateBoilerV1 {

    private boolean empty;
    private boolean boiled;

    public ChocolateBoilerV1() {
        this.empty = true;
        this.boiled = false;
    }

    public void fill(){
        System.out.println("尝试填充");
        if(isEmpty()){
            empty = false;
            boiled = false;
            System.out.println("执行填充。。。。");
        }
    }

    public void drain(){
        System.out.println("尝试排出");
        if(!isEmpty() && isBoiled()){
            empty = true;
            System.out.println("执行排出。。。。。");
        }
    }

    public void boil(){
        System.out.println("尝试煮沸");
        if(!isEmpty() && !isBoiled()){
            boiled = true;
            System.out.println("执行煮沸。。。。。");
        }
    }

    public boolean isEmpty() {
        return empty;
    }

    public boolean isBoiled() {
        return boiled;
    }
}
