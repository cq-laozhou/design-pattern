package com.demo.zyy.pattern.singleton.definition;

/**
 * 经典单例实现
 */
public class Singleton {

    private static Singleton uniqueInstance;

    private Singleton() {
    }

    public static Singleton getInstance(){
        if(null == uniqueInstance){
            uniqueInstance = new Singleton();
        }
        return uniqueInstance;
    }
}
