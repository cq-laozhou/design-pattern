package com.demo.zyy.pattern.singleton.chocolate.v2;

/**
 * 巧克力锅炉-经典模式单例版
 */
public class ChocolateBoilerV2 {

    private static ChocolateBoilerV2 boilerV2;

    private boolean empty;
    private boolean boiled;

    private ChocolateBoilerV2() {
        this.empty = true;
        this.boiled = false;
    }

    public static ChocolateBoilerV2 getInstance(){
        if(boilerV2 == null){
            boilerV2 = new ChocolateBoilerV2();
        }
        return boilerV2;
    }

    public void fill(){
        System.out.println("尝试填充");
        if(isEmpty()){
            empty = false;
            boiled = false;
            System.out.println("执行填充。。。。");
        }
    }

    public void drain(){
        System.out.println("尝试排出");
        if(!isEmpty() && isBoiled()){
            empty = true;
            System.out.println("执行排出。。。。。");
        }
    }

    public void boil(){
        System.out.println("尝试煮沸");
        if(!isEmpty() && !isBoiled()){
            boiled = true;
            System.out.println("执行煮沸。。。。。");
        }
    }

    public boolean isEmpty() {
        return empty;
    }

    public boolean isBoiled() {
        return boiled;
    }
}
