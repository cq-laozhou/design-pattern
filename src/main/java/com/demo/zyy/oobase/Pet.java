package com.demo.zyy.oobase;

/**
 * 宠物抽象
 *  抽象所有宠物共性
 */
public abstract class Pet {

    /**
     * 宠物名称
     *  （假设所有宠物都有名称）
     */
    private String name;

    /**
     * 宠物叫的行为
     * （假设所有宠物都会叫）
     */
    public abstract void call();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
