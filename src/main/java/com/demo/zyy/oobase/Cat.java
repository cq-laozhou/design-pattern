package com.demo.zyy.oobase;

/**
 * 猫
 *  具体的宠物，猫"是一个"宠物，因此继承宠物抽象
 */
public class Cat extends Pet{

    public Cat(String name) {
        //可直接使用父类的方法
        setName(name);
    }

    /**
     * 实现（重写）父类的方法。
     */
    @Override
    public void call() {
        System.out.println("我是"+getName() + ",我的叫声是：喵喵喵");
    }
}
