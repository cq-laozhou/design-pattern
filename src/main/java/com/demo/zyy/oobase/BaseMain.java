package com.demo.zyy.oobase;

/**
 * 测试代码
 */
public class BaseMain {

    public static void main(String[] args) {
        Pet pet = new Cat("加菲");
        pet.call();

        pet = new Dog("黑二");
        pet.call();
    }

}
