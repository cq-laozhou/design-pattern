package com.demo.zyy.oobase;

/**
 * 狗
 *  具体的宠物，狗"是一个"宠物，因此继承宠物抽象
 */
public class Dog extends Pet{

    public Dog(String name) {
        //可直接使用父类的方法
        setName(name);
    }

    /**
     * 实现（重写）父类的方法。
     */
    @Override
    public void call() {
        System.out.println("我是"+getName() + ",我的叫声是：汪汪汪");
    }
}
